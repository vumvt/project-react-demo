const NotData = (props) => {
    const { message } = props;
    return (
        <h2 style={{ color: 'red', textAlign: 'center' }}>{message}</h2>
    );
}

export default NotData;