import React, { Suspense } from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter } from 'react-router-dom';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import { CookiesProvider } from 'react-cookie';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Provider } from 'react-redux';
import store from './data/store';
import '@fortawesome/fontawesome-free/css/all.css';
import "react-pagination-library/build/css/index.css";

toast.configure({
  autoClose: 2000,
  draggable: false,
  position: 'bottom-right',
  hideProgressBar: true,
  newestOnTop: true,
  closeOnClick: true,
  rtl: false,
  pauseOnVisibilityChange: true,
  pauseOnHover: true
});


ReactDOM.render(
  <Provider store={store}>
    <React.StrictMode>
      <CookiesProvider>
        <Suspense fallback={<h1>Loading ...</h1>}>
          <BrowserRouter>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>

              <App />

            </MuiPickersUtilsProvider>
          </BrowserRouter>
        </Suspense>
      </CookiesProvider>
    </React.StrictMode>
  </Provider>
  ,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
