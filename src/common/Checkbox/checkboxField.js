
import { Checkbox, FormControlLabel } from '@material-ui/core';
import { PropTypes } from 'prop-types';
import { useState } from 'react';
import { useEffect } from 'react';
import majorApi from './../../api/majorApi';
import teacherApi from './../../api/teacherApi';

const CheckboxField = (props) => {
    const { field, checked, label, object, color, style, disable, value, onChange } = props;
    const { name } = field;
    const [major, setMajor] = useState({});
    const [teacher, setTeacher] = useState({});

    // const handleChangeInput = (e) => {
    //     const changeEvent = {
    //         target: {
    //             name: name,
    //             value: parseInt(e.target.value)
    //         },
    //     };

    //     field.onChange(changeEvent);
    // }

    useEffect(() => {
        majorApi.getId(object.majorId).then(res => {
            if (res.status === 200) setMajor(res.data);
        });
        teacherApi.getDetail(object.teacherId).then(teacher => {
            if (teacher.status === 200) setTeacher(teacher.data);
        })
    }, []);

    return (
        <tr>
            <td>
                <FormControlLabel
                    control={
                        <Checkbox
                            {...field}
                            name={name}

                            checked={checked}
                            style={style}
                            disabled={disable}
                            onChange={onChange}
                            name={name}
                            color={color}
                            value={value}
                        />
                    }
                // label={label}
                />
            </td>
            <td>
                {object.name}
            </td>
            <td>{major.name}</td>
            <td>{teacher.lastName} {teacher.firstName}</td>
            <td>{object.status === 1 ? 'Đang mở lớp' : ''}</td>
        </tr>
    );
}

CheckboxField.propTypes = {
    field: PropTypes.object.isRequired,
    checked: PropTypes.bool,
    handleChange: PropTypes.func,
    value: PropTypes.string,
    label: PropTypes.string,
}

CheckboxField.defaultProps = {
    color: 'primary'
};

export default CheckboxField;