import React from 'react';
import Radio from '@material-ui/core/Radio';
import { PropTypes } from 'prop-types';
import { FormControlLabel } from '@material-ui/core';


const RadioField = (props) => {
    const {
        field,
        checked,
        label,
        value,
        style
    } = props;
    const { name } = field;

    const handleChangeInput = (e) => {
        const changeEvent = {
            target: {
                name: name,
                value: parseInt(e.target.value)
            },
        };
    
        field.onChange(changeEvent);
    }
    
    return (
        <FormControlLabel
            {...field}
            name={name}
            style={style}
            checked={checked}
            onChange={handleChangeInput}
            value={parseInt(value)}
            label={label}
            control={
                <Radio
                    color={value === '2' ? 'secondary' : 'primary'}
                    size="small"
                />
            }
        />
    );
}

RadioField.propTypes = {
    field: PropTypes.object.isRequired,
    checked: PropTypes.bool,
    handleChange: PropTypes.func,
    value: PropTypes.string,
    label: PropTypes.string,
}

RadioField.defaultProps = {
};

export default RadioField;