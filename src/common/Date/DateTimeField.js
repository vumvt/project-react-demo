import React, { useEffect, useState } from 'react';
import { KeyboardDatePicker } from "@material-ui/pickers";
import { FormGroup } from 'reactstrap';
import PropTypes from 'prop-types';

const DateTimeField = (props) => {
    const {
        field,
        form,

        label,
        format,
        placeholder,
        onChange
    } = props;
    const { name, value } = field;
    const { errors, touched } = form;
    const showError = errors[name] && touched[name];
    const [errorMessage, setErrorMessage] = useState('');
    const [selectedDate, setSelectedDate] = React.useState(null);

    useEffect(() => {
        setSelectedDate(value);
    }, [value]);

    useEffect(() => {
        if (errors[name] && touched[name]) {
            setErrorMessage(errors[name]);
        } else {
            setErrorMessage('');
        }
    }, [errors[name] && touched[name]]);

    const handleChangeDate = (date) => {

        const changeEvent = {
            target: {
                name: name,
                value: date
            }
        };
        field.onChange(changeEvent);
        onChange && onChange(date);
    }

    return (
        <FormGroup>
            <KeyboardDatePicker
                name={name}
                {...field}

                error={showError}
                clearable
                label={label}
                value={selectedDate}
                onChange={handleChangeDate}
                placeholder={placeholder}
                minDate={new Date()}
                format={format}
                helperText={errorMessage}
            />
        </FormGroup>

    );
}

DateTimeField.propTypes = {
    field: PropTypes.object.isRequired,
    form: PropTypes.object.isRequired,
    label: PropTypes.string,
    format: PropTypes.string,
    placeholder: PropTypes.string,
    onChange: PropTypes.func,
}
DateTimeField.defaultProps = {
    label: '',
    placeholder: '',
    onChange: null,
}

DateTimeField.defaultProps = {}

export default DateTimeField;