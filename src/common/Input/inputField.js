import { TextField } from "@material-ui/core";
import { useEffect, useState } from "react";
import PropTypes from 'prop-types';
import { FormGroup } from "reactstrap";


const InputField = (props) => {
    const {
        field,
        form,
        label,
        placeholder,
        type,
        onChange,
        style,
        rows,
        min,
        max,
        variant,
        multiline,
        className,
        fullWidth,
        InputProps,
        disabled,
        defaultValue
    } = props;
    const { name } = field;
    const { errors, touched } = form;
    const showError = errors[name] && touched[name];
    const [errorMessage, setErrorMessage] = useState('');

    const handleChangeInput = (value) => {
        const changeEvent = {
            target: {
                name: name,
                value: value
            },
        };

        field.onChange(changeEvent);

        onChange && onChange(value);
    }

    useEffect(() => {
        if (errors[name] && touched[name]) {
            setErrorMessage(errors[name]);
        } else {
            setErrorMessage('');
        }
    }, [errors, touched]);

    return (
        <FormGroup>
            <TextField
                {...field}
                name={name}

                error={showError}
                style={style}
                label={label}
                placeholder={placeholder}
                type={type}
                fullWidth={fullWidth}
                rows={rows}
                multiline={multiline}
                min={min}
                max={max}
                defaultValue={defaultValue}
                helperText={errorMessage}
                InputProps={InputProps}
                disabled={disabled}
               
                className={className}
                variant={variant}
                onChange={(event) => handleChangeInput(event.target.value)}
            />
        </FormGroup>

    );
}

InputField.propTypes = {
    field: PropTypes.object.isRequired,
    form: PropTypes.object.isRequired,

    onChange: PropTypes.func,
    label: PropTypes.string,
    type: PropTypes.string,
    placeholder: PropTypes.string,
    rows: PropTypes.number,
    min: PropTypes.number,
    max: PropTypes.number,
    variant: PropTypes.string,
    fullWidth: PropTypes.bool,
    style: PropTypes.object,
}

InputField.defaultProps = {
    type: 'text',
    label: '',
    placeholder: '',
    onChange: null,
    variant: '',
    fullWidth: true,
    multiline: null,
}

InputField.defaultProps = {};

export default InputField;