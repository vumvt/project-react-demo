import { FormControl, FormHelperText, InputLabel, MenuItem, Select } from "@material-ui/core";
import { useEffect, useState } from "react";

const SelectField = (props) => {
    const {
        field,
        form,

        label,
        options,
        onSelected,
        multiple,
        className,
        fullWidth,
        style,
        check
    } = props;

    // name , values, handleChange, handleBlur
    const { name, value } = field;
    const { errors, touched } = form;
    const showError = errors[name] && touched[name];
    const [errorMessage, setErrorMessage] = useState('');


    const [selectedOption, setSelectedOption] = useState('');

    useEffect(() => {
        let initOption = options.find(o => o.value === value);
        setSelectedOption(initOption);
    }, [options]);

    useEffect(() => {
        errors[name] && touched[name]
            ? setErrorMessage(errors[name])
            : setErrorMessage('');

    }, [errors, touched]);

    useEffect(() => {
        setSelectedOption(value);
    }, [value]);

    const handleSelectedOptionChange = (event) => {
        let selectedValue;
        setSelectedOption(event.target.value);
        selectedValue = event.target.value ? event.target.value : '';

        const changeEvent = {
            target: {
                name: name,
                value: selectedValue
            }
        };

        field.onChange(changeEvent);
        onSelected && onSelected(selectedValue);
    }

    return (
        <FormControl className={className} error={showError} fullWidth>
            <InputLabel id={name}>{label}</InputLabel>
            <Select
             // name , values, handleChange, handleBlur
                {...field}

                id={name}
                labelId={name}
                multiple={multiple}
                fullWidth={fullWidth}
                value={selectedOption}
                onChange={handleSelectedOptionChange}
                style={style}
            >
                {check ? options.map((item) => (
                    <MenuItem key={item.id} value={item.id}>
                        {item.name}
                    </MenuItem>
                )) : options.map((item) => (
                    <MenuItem key={item.id} value={item.id}>
                        {item.lastName} {item.firstName}
                    </MenuItem>
                ))}
            </Select>
            {
                showError && <FormHelperText>{errorMessage}</FormHelperText>
            }
        </FormControl>
    );
}

export default SelectField;