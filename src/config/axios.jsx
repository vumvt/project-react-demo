import axios from 'axios';
import queryString from 'query-string';
import cookies from './cookies';

// const tokenCookie = Cookies.get('token');
// const data = { ...JSON.parse(tokenCookie) };

const axiosClient = axios.create({
    baseURL: process.env.REACT_APP_URL_API,
    headers: {
        "Content-Type": "application/json",
    },
    // withCredentials: true,
    paramsSerializer: params => queryString.stringify(params),
});

axiosClient.interceptors.request.use(config => {
    const tokenCookie = cookies.getCookie('token');
    if (tokenCookie) {
        // const data = { ...JSON.parse(tokenCookie) };
        config.headers['Authorization'] = `Bearer ${tokenCookie}`
    }
    return config;
});

axiosClient.interceptors.response.use((response) => {
    if (response && response.data) {
        return response.data;
    }
    return response;
}
    , (error) => {
        if (error.response.status === 401) {
            window.location.href = "/login";
        }
    }
);

export default axiosClient;