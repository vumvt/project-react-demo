
import Cookies from 'js-cookie';

const cookies = {

    getCookie: (name) => {
        if(name) {
            return Cookies.get(name);
        }
    },
    setCookie: (name, value, date) => {
        if(name && value !== null) {
            return Cookies.set(name, value, { expires: parseInt(date) });
        }
    },
    removeCookie: (name) => {
        if(name) {
            return Cookies.remove(name);
        }
    }
}

export default cookies;