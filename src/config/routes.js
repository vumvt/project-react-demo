import Home from './../features/Default-Layout/Home/home';
import Major from './../features/Major/index';
import Student from './../features/Student/index';
import Teacher from './../features/Teacher/index';
import Login from './../features/Login/login';
import SignUp from '../features/SignUp/signUp';
import Subject from './../features/Subject/index';
import SubjectRegistration from './../features/Subject-Registration/index';

const routes = [
    { path: "/", exact: true, component: Home, name: "Home" },
    { path: "/home", exact: true, component: Home, name: "Home" },
    { path: "/major", exact: false, component: Major, name: "Major" },
    { path: "/student", exact: false, component: Student, name: "Student" },
    { path: "/teacher", exact: false, component: Teacher, name: "Teacher" },
    { path: "/login", exact: true, component: Login, name: "Login" },
    { path: "/signUp", exact: true, component: SignUp, name: "SignUp" },
    { path: "/subject", exact: false, component: Subject, name: "Subject" },
    { path: "/register", exact: true, component: SubjectRegistration, name: "SubjectRegistration" },

];

export default routes;