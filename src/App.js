import { Fragment } from 'react';
import './App.css';
import DefaultLayout from './features/Default-Layout/default-layout';
import Login from './features/Login/login';
import { Route, Switch } from 'react-router-dom';
import SignUp from './features/SignUp/signUp';
import NotFound from './page/notFound';

function App() {
  return (
    <Fragment>
      <Switch>
        <Route exact path="/login" name="Login page" component={Login} />
        <Route exact path="/signUp" name="Sign Up" component={SignUp} />
        <Route path="/" name="Home" component={DefaultLayout} />
        <Route path="/not-found" name="Not Found" component={NotFound} />


      </Switch>
    </Fragment>

  );
}

export default App;


// npm i formik
// npm i reactstrap
// npm i react-router-dom
// npm i yup
// npm i react-redux
// npm i bootstrap
// npm install @material-ui/core
// npm install @material-ui/icons
// npm install @reduxjs/toolkit