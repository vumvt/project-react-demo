import { Container, Grid, makeStyles } from "@material-ui/core";
import { Fragment, useEffect, useState } from "react";
import { Modal, ModalBody, ModalHeader } from "reactstrap";
import MajorForm from './../components/MajorForm/MajorForm';
import majorApi from './../../../api/majorApi';
import gradeApi from './../../../api/gradeApi';
import MajorList from './../components/MajorList/MajorList';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import { toast } from "react-toastify";
import { ERR_MAJOR_EXISTING } from './../../../config/APIStatus';

const useStyles = makeStyles((theme) => ({
    container: {
        display: 'grid',
        gridGap: theme.spacing(3),

    },
    buttonAdd: {
        float: "right",
        color: "#CC0000",
        marginTop: "10px",
        marginBottom: "10px",
        fontSize: '37px'

    }
}));
const MainPage = () => {
    const classes = useStyles();
    const [isOpen, setIsOpen] = useState(false);
    const [action, setAction] = useState(false);
    const [isChange, setIsChange] = useState(false);
    const [selectedMajor, setSelectedMajor] = useState({
        name: '',
        code: '',
        description: '',
        gradeId: ''
    });
    const [majors, setMajors] = useState([]);
    const [grades, setGrades] = useState([]);

    const showModal = () => {
        setIsOpen(true);
        setAction(true);
    }

    const hideModal = () => {
        setIsOpen(false);
        setSelectedMajor({
            name: '',
            code: '',
            description: '',
            gradeId: ''
        })
    }

    const loadingMajor = () => {
        majorApi.getAll().then(res => {
            if (res.status === 200) {
                setMajors(res.data);
            }
        }).catch(err => {
            console.log(err);
        });
    }

    const loadingGrade = () => {
        gradeApi.getAll().then((res) => {
            setGrades(res.data);
        }).catch(err => {
            console.log(err);
        });
    }

    useEffect(() => {
        loadingMajor();
    }, [isChange]);


    useEffect(() => {
        loadingGrade();
    }, []);

    const handleOnEditMajor = async (major) => {
        setIsOpen(true);
        setAction(false);
        try {
            const majorResponse = await majorApi.getId(major.id);
            setSelectedMajor(majorResponse.data);
        } catch (error) {
            console.log('Error when response teacher id ', error);
        }
    }

    const handleOnRemoveMajor = (id) => {
        majorApi.delete(id).then(res => {
            if (res.status === 200) {
                toast.success("✔️ Xóa thành công rồi nha 😀");
                setIsChange(!isChange);
            }else {
                toast.error(ERR_MAJOR_EXISTING.message);
            }
        });
    }

    const onSubmit = (value) => {
        if (!value.id) {
            majorApi.add(value).then((res) => {
                if (res.status === 200) {
                    setIsChange(!isChange);
                    toast.success("✔️ Add successfully.!");
                }
            }).catch(err => {
                console.log('Error request data : ', err);
                toast.error("❗ Bad Request.!");
            });
        } else {
            console.log(value);
            majorApi.update(value).then(res => {
                if (res.status === 200) {
                    setIsChange(!isChange);
                    toast.success("✔️ Update successfully.!");
                } else {
                    toast.error("❗ Update Faild.!");
                }
            })
        }

        hideModal();

    }
    return (
        <Fragment>
            {/* <Container fixed> */}
            <div style={{marginRight: '30px', marginLeft: '30px'}}>
                <Grid container={true} justify="space-between" spacing={3}>
                    <Grid item xs={12}>
                        <a onClick={showModal} style={{cursor: 'pointer'}}>
                            <AddCircleOutlineIcon className={classes.buttonAdd} />
                        </a>
                    </Grid>
                </Grid>
                <MajorList
                    majors={majors}
                    handleOnEditMajor={handleOnEditMajor}
                    handleOnRemoveMajor={handleOnRemoveMajor}
                />
            {/* </Container> */}
            </div>
            <Modal isOpen={isOpen}>
                <ModalHeader toggle={hideModal}>
                    {action ? 'Add New Major' : 'Update Major'}
                </ModalHeader>
                <ModalBody>
                    {isOpen &&
                        <MajorForm
                            action={action}
                            initValues={selectedMajor}
                            onSubmit={onSubmit}
                            grades={grades}
                        />
                    }

                </ModalBody>
            </Modal>
        </Fragment>
    );
}

export default MainPage;