import { Route, Switch, useRouteMatch } from "react-router-dom";
import MainPage from './pages/index';

const Major = () => {
    const match = useRouteMatch();
    return (
        <Switch>
            <Route path={match.url} component={MainPage} />
        </Switch>
    );
}

export default Major;