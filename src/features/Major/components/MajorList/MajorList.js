import React, { Fragment, useState } from 'react';
import { Container, Grid, makeStyles } from '@material-ui/core';
import { Table } from 'reactstrap';
import MajorItem from '../MajorItem/MajorItem';

const useStyles = makeStyles((theme) => ({
    container: {
        display: 'grid',
        gridGap: theme.spacing(3),

    },
    buttonAdd: {
        float: "right",
        color: "#CC0000",
        marginTop: "10px",
        marginBottom: "10px",
        fontSize: '37px'

    },
    text: {
        textAlign: "center"
    },
    divider: {
        margin: theme.spacing(2, 0),
    },
}));

const MajorList = (props) => {
    const { majors, handleOnEditMajor, handleOnRemoveMajor } = props;
    const classes = useStyles();
    return (
        <Fragment>
            <Table size={'sm'} striped bordered className={classes.text}>
                <thead style={{backgroundColor: '#DAA520'}}>
                    <tr>
                        <th>Stt.</th>
                        <th>Tên môn học</th>
                        <th>Mã số</th>
                        <th>Khối</th>
                        <th>Thông tin</th>
                        <th>Chỉnh sửa</th>
                        <th>Xóa</th>
                    </tr>
                </thead>
                <tbody>
                    {majors.map((major, index) => (
                    <MajorItem
                        index={index}
                        key={major.id}
                        major={major}
                        handleOnEdit={handleOnEditMajor}
                        handleOnRemove={handleOnRemoveMajor}
                    />
                ))}
                </tbody>
            </Table>
        </Fragment>
    );
}

export default MajorList;