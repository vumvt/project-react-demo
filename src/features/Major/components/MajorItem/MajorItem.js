import gradeApi from "../../../../api/gradeApi";
import { useState, useEffect } from 'react';
import EditIcon from '@material-ui/icons/Edit';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
const MajorItem = (props) => {
    const {major, handleOnEdit, handleOnRemove, index} = props;
    const [grade, setGrade] = useState('');

    useEffect(() => {
        gradeApi.getId(major.gradeId).then((res) => {
            if(res.status === 200) setGrade(res.data);
        }).catch(err => {
            console.log('Error when response grade object', err);
        })
    }, []);

    const handleOnEditClick = (event) => {
        event.preventDefault();
        if (handleOnEdit) handleOnEdit(major);
    }

    const handleOnRemoveClick = (event) => {
        event.preventDefault();
        if (handleOnRemove) handleOnRemove(major.id);
    }
    return ( 
        <tr>
            <th>{index + 1}</th>
            <td>{major.name}</td>
            <td>{major.code}</td>
            <td>{grade.name}</td>
            <td>{major.description}</td>
            <td>
                <a onClick={handleOnEditClick}><i className="fas fa-edit" style={{color: 'green', cursor: 'pointer'}}></i></a>
            </td>
            <td>
                <a onClick={handleOnRemoveClick}><DeleteForeverIcon style={{color: '#B22222', cursor: 'pointer'}}/></a>
            </td>
        </tr>
     );
}
 
export default MajorItem;