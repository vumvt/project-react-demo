import React, { Fragment } from "react";
import * as Yup from "yup";
import SelectField from './../../../../common/Select/selectField';
import InputField from './../../../../common/Input/inputField';
import SaveIcon from '@material-ui/icons/Save';
import RotateLeftIcon from '@material-ui/icons/RotateLeft';
import { Grid, Button } from '@material-ui/core';
import { FastField, Form, Formik } from 'formik';

const MajorForm = (props) => {
    const { initValues, onSubmit, grades, action } = props;

    const schema = Yup.object().shape({
        name: Yup.string().required('This field is required !').max(20, 'This name max length is 20 !'),
        code: Yup.string().required('This field is required !').max(10, 'This code max length is 10 !'),
        gradeId: Yup.number().required('This field is required !').nullable(),
        description: Yup.string().notRequired()
    });


    return (
        <Formik
            validationSchema={schema}
            initialValues={initValues}
            onSubmit={onSubmit}
            enableReinitialize={true}
        >
            {({ values, isValid, errors, resetForm }) => {
                return (
                    <Fragment>
                        <Form
                            autoComplete="off"
                        >
                            <Grid container spacing={3}>
                                <Grid item xs={12}>
                                    <FastField
                                        component={InputField}
                                        name="name"

                                        placeholder="Enter your major name !"
                                        fullWidth={true}
                                        label="Major name *"
                                    />
                                </Grid>
                                <Grid item xs={6}>
                                    <FastField
                                        component={InputField}
                                        name="code"

                                        placeholder="Enter your major code !"
                                        fullWidth={true}
                                        label="Major code *"
                                    />
                                </Grid>
                                <Grid item xs={6}>
                                    <FastField
                                        component={SelectField}
                                        name="gradeId"

                                        options={grades}
                                        check={true}
                                        label="Grade *"
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <FastField
                                        component={InputField}
                                        name="description"

                                        placeholder="Enter your major description !"
                                        fullWidth={true}
                                        label="Description "
                                        rows={5}
                                        fullWidth={true}
                                        multiline
                                    />
                                </Grid>
                                <p className="text-danger text-center">* not be empty</p>
                                <hr />
                                <Grid item xs={12}>
                                    <Button
                                        variant="contained"
                                        color="secondary"
                                        size="small"
                                        startIcon={<RotateLeftIcon />}
                                        onClick={resetForm}
                                    >
                                        Reset
                                    </Button>
                                    <div className="float-right">
                                        <Button
                                            variant="contained"
                                            color="primary"
                                            size="small"
                                            type="submit"
                                            disabled={!isValid}
                                            className="mr-2"
                                            startIcon={<SaveIcon />}
                                        >
                                            {action ? 'Save' : 'Update'}
                                        </Button>
                                    </div>

                                </Grid>
                            </Grid>
                        </Form>
                    </Fragment>
                )
            }}
        </Formik>

    );
}

export default MajorForm;