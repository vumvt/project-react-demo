import { Grid, makeStyles } from "@material-ui/core";
import { Fragment, useEffect, useState } from "react";
import teacherApi from "../../../../api/teacherApi";
import TeacherList from "../../components/TeacherList/TeacherList";
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import React from 'react'
import { Modal, ModalBody, ModalHeader } from "reactstrap";
import TeacherForm from '../../components/TeacherForm/TeacherForm';
import SearchForm from "../../../form/SearchForm";
import { toast } from "react-toastify";
import { ERR_ADD, ERR_TEACHER_IS_TEACHING, ERR_TEACHER_NOT_FOUND, ERR_UPDATE, SUCCESS_ADD } from "../../../../config/APIStatus";
import subjectApi from './../../../../api/subjectApi';
import { SUCCESS_DELETE, SUCCESS_UPDATE } from './../../../../config/APIStatus';

const useStyles = makeStyles((theme) => ({
    container: {
        display: 'grid',
        gridGap: theme.spacing(3),

    },
    buttonAdd: {
        float: "right",
        color: "#CC0000",
        marginTop: "10px",
        marginBottom: "10px",
        fontSize: '37px'

    }
}));

const MainPage = () => {
    const [teachers, setTeachers] = useState([]);
    const [action, setAction] = useState();
    const classes = useStyles();
    const [isOpen, setIsOpen] = useState(false);
    const [isChange, setIsChange] = useState(false);
    const [subjects, setSubjects] = useState([]);
    const [seletedTeacher, setSeletedTeacher] = useState(
        {
            lastName: '',
            firstName: '',
            phone: '',
            code: '',
            gender: 0
        }
    );

    const loadingTeacher = async () => {
        try {
            const teachersResponse = await teacherApi.getAll();
            setTeachers(teachersResponse.data);
        } catch (error) {
            console.log('Error when response teacher list ', error);
        }
    }
    const loadingSubjects = () => {
        subjectApi.getAll().then(res => {
            if (res.status === 200)
                setSubjects(res.data);
        }).catch(err => console.log(err));
    }

    useEffect(() => { loadingSubjects() }, []);

    useEffect(() => {
        loadingTeacher();
    }, [isChange]);

    const handleOnEdit = async (id) => {
        setIsOpen(true);
        setAction(false);
        try {
            const teacherResponse = await teacherApi.getDetail(id);
            setSeletedTeacher(teacherResponse.data);
        } catch (error) {
            console.log('Error when response teacher id ', error);
        }

    }

    const handleOnRemoveTeacher = (id) => {

        teacherApi.deleteId(id).then(res => {
            if (res.status === 200) {
                setIsChange(!isChange);
                toast.success(SUCCESS_DELETE.message);
            } else {
                toast.error(ERR_TEACHER_IS_TEACHING.message);
            }
        });
    }
    const onSubmit = (teacher) => {
        if (!teacher.id) {
            teacherApi.addNew(teacher).then(res => {
                if (res.status === 200) {
                    setIsChange(!isChange);
                    toast.success(SUCCESS_ADD.message);
                } else {
                    toast.error(ERR_ADD.message);
                }
            });
        } else {
            teacherApi.update(teacher).then(res => {
                if (res.status === 200) {
                    setIsChange(!isChange);
                    toast.success(SUCCESS_UPDATE.message);
                } else {
                    toast.error(ERR_UPDATE.message);
                }
            });

        }
        hideMoal();
    }

    const showMoal = () => {
        setIsOpen(true);
        setAction(true);
    }
    const hideMoal = () => {
        setIsOpen(false);
        setSeletedTeacher({
            lastName: '',
            firstName: '',
            phone: '',
            code: '',
            gender: 1
        });
    }

    const onSearch = (data) => {
        const params = { search_key: data.searchKey };
        teacherApi.searchKey(params).then(res => {
            if (res.status !== 200) {
                toast.error(`${ERR_TEACHER_NOT_FOUND.message}`);
            } else {
                setTeachers(res.data);
            }
        })

    }
    return (
        <Fragment>
            {/* <Container fixed> */}
            <div style={{marginRight: '30px', marginLeft: '30px'}}>
                <Grid container={true} justify="space-between" spacing={3}>
                    <Grid item xs={6}>
                        <SearchForm
                            initValues={{
                                searchKey: ''
                            }}
                            onSubmit={onSearch}
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <a onClick={showMoal} style={{ cursor: 'pointer' }}><AddCircleOutlineIcon className={classes.buttonAdd} /></a>
                    </Grid>
                </Grid>

                <TeacherList
                    teachers={teachers}
                    handleOnRemoveTeacher={handleOnRemoveTeacher}
                    handleOnEdit={handleOnEdit}
                />
            {/* </Container> */}
            </div>

            <Modal isOpen={isOpen}>
                <ModalHeader toggle={hideMoal}>
                    {action ? 'Thêm mới giảng viên' : 'Thay đổi thông tin giảng viên'}
                </ModalHeader>
                <ModalBody>
                    {isOpen &&
                        <TeacherForm
                            action={action}
                            initValue={seletedTeacher}
                            onSubmit={onSubmit}
                        />
                    }

                </ModalBody>
            </Modal>
        </Fragment>
    );

}

export default MainPage;