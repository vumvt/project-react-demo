import React, { Fragment } from 'react';
import { makeStyles } from '@material-ui/core';
import { Table } from 'reactstrap';
import TeacherItem from '../TeacherItem/TeacherItem';
import { PropTypes } from 'prop-types';

const useStyles = makeStyles((theme) => ({
    container: {
        display: 'grid',
        gridGap: theme.spacing(3),

    },
    buttonAdd: {
        float: "right",
        color: "#CC0000",
        marginTop: "10px",
        marginBottom: "10px",
        fontSize: '37px'

    },
    text: {
        textAlign: "center"
    },
    divider: {
        margin: theme.spacing(2, 0),
    },
}));

const TeacherList = (props) => {
    const { teachers, handleOnEdit, handleOnRemoveTeacher } = props;
    const classes = useStyles();
    console.log(teachers);
    return (
        <Fragment>

            <Table striped bordered className={classes.text} size={'sm'}>
                <thead style={{backgroundColor: '#DAA520'}}>
                    <tr>
                        <th>Stt.</th>
                        <th>Họ và tên</th>
                        <th>Mã số</th>
                        <th>Giới tính</th>
                        <th>Số điện thoại</th>
                        <th>Chỉnh sửa</th>
                        <th>Xóa</th>
                    </tr>
                </thead>
                <tbody>
                    {teachers.map((teacher, index) => (
                        <TeacherItem
                            index={index}
                            key={teacher.id}
                            teacher={teacher}
                            onEditClick={handleOnEdit}
                            onRemoveClick={handleOnRemoveTeacher}
                        />
                    ))}
                </tbody>
            </Table>
        </Fragment>
    );
}
TeacherList.propsType = {
    teachers: PropTypes.array,
}
TeacherList.defaultProps = {
    teachers: []
}

export default TeacherList;