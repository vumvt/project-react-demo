import { FormLabel, Grid, Button } from '@material-ui/core';
import { FastField, Field, Form, Formik } from 'formik';
import React, { Fragment } from 'react';
import InputField from '../../../../common/Input/inputField';
import RadioField from '../../../../common/Radio/radioField';
import SelectField from '../../../../common/Select/selectField';
import * as Yup from "yup";
import SaveIcon from '@material-ui/icons/Save';
import RotateLeftIcon from '@material-ui/icons/RotateLeft';


const TeacherForm = (props) => {
    const { initValue, onSubmit, majors, action } = props;

    //format phone
    const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/

    const validationSchema = Yup.object().shape({
        lastName: Yup.string().required('Không được bỏ trống !').max(20, 'Max length last name is 20 !'),
        firstName: Yup.string().required('Không được bỏ trống !').max(20, 'Max length first name is 20 !'),
        code: Yup.string().required('Không được bỏ trống !').max(10, 'Max length code is 10 !'),
        phone: Yup.string().required('Không được bỏ trống !').max(10, 'Số điện thoại không hợp lệ !').matches(phoneRegExp, 'Số điện thoại không hợp lệ !'),
        gender: Yup.number().required('Không được bỏ trống !')
    })

    return (

        <Formik
            initialValues={initValue}
            onSubmit={onSubmit}
            validationSchema={validationSchema}
            enableReinitialize={true}
        >

            {({ isValid, values, resetForm }) => {
                return (
                    <Fragment>
                        <Form autoComplete="off" aria-autocomplete="none">
                            <Grid container={true} justify="space-between" spacing={3}>

                                <Grid item xs={6}>
                                    <FastField
                                        component={InputField}
                                        name="lastName"

                                        label="Họ *"
                                        placeholder="Điền họ của giảng viên"
                                        fullWidth={true}
                                    />
                                </Grid>
                                <Grid item xs={6}>
                                    <FastField
                                        component={InputField}
                                        name="firstName"

                                        label="Tên *"
                                        placeholder="Điền tên của giảng viên "
                                        fullWidth={true}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <FastField
                                        component={InputField}
                                        name="code"

                                        label="Mã số giảng viên *"
                                        placeholder="Điền mã số của giảng viên"
                                        fullWidth={true}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <FastField
                                        component={InputField}
                                        name="phone"

                                        label="Số điện thoại *"
                                        placeholder="Điền số điện thoại của giảng viên"
                                        fullWidth={true}
                                    />
                                </Grid>
                                <Grid item={true} xs={12}>
                                    <FormLabel component="legend" style={{ fontSize: '15px' }}>Giới tính *</FormLabel>
                                    <Field
                                        component={RadioField}
                                        name="gender"

                                        checked={values.gender === 0}
                                        value='0'
                                        label="Nam"
                                    />
                                    <Field
                                        component={RadioField}
                                        name="gender"

                                        checked={values.gender === 1}
                                        value='1'
                                        label="Nữ"
                                    />
                                    <Field
                                        component={RadioField}
                                        name="gender"

                                        checked={values.gender === 2}
                                        value='2'
                                        label="Khác"
                                    />
                                </Grid>

                                <hr />

                                <Grid item xs={12}>
                                    <Button
                                        variant="contained"
                                        color="secondary"
                                        size="small"
                                        // className="mr-2"
                                        startIcon={<RotateLeftIcon />}
                                        onClick={resetForm}
                                    >
                                        Reset
                                    </Button>
                                    <div className="float-right">
                                        <Button
                                            variant="contained"
                                            color="primary"
                                            size="small"
                                            type="submit"
                                            disabled={!isValid}
                                            className="mr-2"
                                            startIcon={<SaveIcon />}
                                        >
                                            {action ? 'Lưu' : 'Cập nhật'}
                                        </Button>
                                        {/* <Button
                                            variant="contained"
                                            color="default"
                                            size="small"
                                            onClick={resetForm}
                                            // className={classes.button}
                                            startIcon={<HighlightOffIcon />}
                                        >
                                            Cancle
                                        </Button> */}
                                    </div>

                                </Grid>

                            </Grid>
                        </Form>
                    </Fragment>
                )
            }}
        </Formik>
    );
}
export default TeacherForm;