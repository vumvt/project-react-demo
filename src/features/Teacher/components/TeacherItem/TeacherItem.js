import React, { useEffect, useState } from 'react';
import EditIcon from '@material-ui/icons/Edit';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';

const TeacherItem = (props) => {
    const { teacher, onEditClick, onRemoveClick, index } = props;
    const [gender, setGender] = useState('');

    const handleOnEditClick = (event) => {
        event.preventDefault();
        if (onEditClick) onEditClick(teacher.id);
    }

    const handleOnRemoveClick = (event) => {
        event.preventDefault();
        if (onRemoveClick) onRemoveClick(teacher.id);
    }
    useEffect(() => {
        if (teacher.gender === 0)
            setGender('Nam');
        else if (teacher.gender === 1)
            setGender('Nữ');
        else
            setGender('Khác');
    }, []);


    return (
        <tr>
            <th>{index + 1}</th>
            <td>{teacher.lastName} {teacher.firstName}</td>
            <td>{teacher.code}</td>
            <td>{gender}</td>
            <td>{teacher.phone}</td>
            <td>
                <a onClick={handleOnEditClick}><i className="fas fa-edit" style={{color: 'green', cursor: 'pointer'}}></i></a>
            </td>
            <td>
                <a onClick={handleOnRemoveClick}><DeleteForeverIcon style={{color: '#B22222', cursor: 'pointer'}}/></a>
            </td>
        </tr>
    )

}
export default TeacherItem;