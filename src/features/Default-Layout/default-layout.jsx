import React, { Fragment } from 'react';
import { Switch } from 'react-router-dom';
import { Route } from 'react-router-dom';
import routes from './../../config/routes';
import NotFound from './../../page/notFound';
import Header from "./Header/header";
import Footer from './Footer/footer';

const DefaultLayout = () => {
    return (
        <Fragment>
            <Header />
            
            <Switch>
                {routes.map((route, idx) => {
                    return route.component
                        ? (
                            <Route key={idx} path={route.path} exact={route.exact} component={route.component} name={route.name} />
                        )
                        : null;
                })
                }
                {/* <Route component={NotFound} name="Not Found" /> */}
            </Switch>
        </Fragment>
    );
}

export default DefaultLayout;