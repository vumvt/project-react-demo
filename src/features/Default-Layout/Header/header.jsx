import { Avatar, makeStyles, withStyles, Container } from '@material-ui/core';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import AccountCircleRoundedIcon from '@material-ui/icons/AccountCircleRounded';
import ExitToAppRoundedIcon from '@material-ui/icons/ExitToAppRounded';
import React, { useState } from 'react';
import { NavLink, useHistory } from 'react-router-dom';
import image from '../../../asset/logo192.png';
import avatar1 from '../../../asset/IMG_3555.JPG';

import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavbarText
} from 'reactstrap';
import cookies from './../../../config/cookies';




const StyledMenu = withStyles({
    paper: {
        border: '1px solid #d3d4d5',
    },
})((props) => (
    <Menu
        elevation={0}
        getContentAnchorEl={null}
        anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
        }}
        transformOrigin={{
            vertical: 'top',
            horizontal: 'center',
        }}
        {...props}
    />
));

const StyledMenuItem = withStyles((theme) => ({
    root: {
        '&:focus': {
            backgroundColor: theme.palette.primary.main,
            '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
                color: theme.palette.common.white,
            },
        },
    },
}))(MenuItem);



const useStyles = makeStyles(() => ({
    navlink: {
        color: 'black',
        marginRight: '20px',
        fontSize: '20px',
        '&:hover': {
            color: 'blue',
            textDecoration: 'none'
        },
        fontWeight: "500"
    },
    image: {
        borderRadius: '40px',
        marginRight: '10px'
    }

}));

const Header = () => {
    const classes = useStyles();
    const [isOpen, setIsOpen] = useState(false);
    const history = useHistory();

    const toggle = () => setIsOpen(!isOpen);
    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleClick = (event) => {
        event.preventDefault();
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const handleClickLogOut = () => {
        history.push('/login');
        cookies.removeCookie('token');
        cookies.removeCookie('userInfo');
    }


    const handleClickSignUp = () => {
        history.push('/signUp');
    }
    return (
        <div>
            <Navbar expand="md" style={{ backgroundColor: '#40E0D0' }}>
                <Container>
                    {/* <NavbarBrand href="/">

                        <NavLink to="/home">
                            <img className={classes.image} src={image} width={'50'} height={'40'} />
                        </NavLink>
                    </NavbarBrand> */}
                    <NavbarToggler onClick={toggle} />
                    <Collapse isOpen={isOpen} navbar>
                    <NavLink to="/home">
                            <img className={classes.image} src={image} width={'50'} height={'40'} />
                        </NavLink>
                        <Nav className="mr-auto" navbar>
                            <NavItem>
                                <NavLink to="/home" className={classes.navlink} activeStyle={{ color: '#FF6347' }}>Trang chủ</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink to="/student" activeStyle={{ color: '#FF6347' }} className={classes.navlink}>Sinh viên</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink to="/teacher" activeStyle={{ color: '#FF6347' }} className={classes.navlink}>Giảng viên</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink to="/major" activeStyle={{ color: '#FF6347' }} className={classes.navlink}>Chuyên ngành</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink to="/subject" activeStyle={{ color: '#FF6347' }} className={classes.navlink}>Môn học</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink to="/register" activeStyle={{ color: '#FF6347' }} className={classes.navlink}>Đăng ký học phần</NavLink>
                            </NavItem>
                        </Nav>


                        <NavbarText>
                            {/* <NavLink to="/login"> */}
                            <a onClick={handleClick} style={{ cursor: 'pointer' }}>
                                <Avatar variant="rounded" alt="Cindy Baker" src={avatar1} />
                            </a>
                            <StyledMenu
                                id="customized-menu"
                                anchorEl={anchorEl}
                                keepMounted
                                open={Boolean(anchorEl)}
                                onClose={handleClose}
                            >
                                <StyledMenuItem onClick={handleClickSignUp}>
                                    <ListItemIcon>
                                        <AccountCircleRoundedIcon fontSize="small" />
                                    </ListItemIcon>
                                    <ListItemText primary="Đăng ký" />
                                </StyledMenuItem>
                                <StyledMenuItem onClick={handleClickLogOut}>
                                    <ListItemIcon>
                                        <ExitToAppRoundedIcon fontSize="small" />
                                    </ListItemIcon>
                                    <ListItemText primary="Đăng xuất" />
                                </StyledMenuItem>

                            </StyledMenu>

                            {/* </NavLink> */}
                        </NavbarText>
                    </Collapse>
                </Container>
            </Navbar>
        </div >
    );
}

export default Header;