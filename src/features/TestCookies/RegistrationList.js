import { FastField, Form, Formik } from 'formik';
import React, { Fragment, useEffect, useState } from 'react';
import { useCookies } from 'react-cookie';
import { Button, Col, Container, Row } from 'reactstrap';
import * as Yup from 'yup';


const RegistrationList = () => {
    const [cookies, setCookie, removeCookie] = useCookies(['result']);
    const [resultCookie, setResultCookie] = useState(0);

    useEffect(() => {
        console.log(resultCookie);
        if (cookies.result === undefined) return {}
        else return setResultCookie(cookies.result);
    }, [resultCookie]);

    const onSubmit = (value) => {
        let result = (parseInt(value.textA) + parseInt(value.textB));
        setResultCookie(setCookie('result', JSON.parse(result), { path: '/' }));
    }

    return (
        <Fragment>

            <Container className="mt-4">
                <h2 className="text-center">Learning About React - Cookie</h2>
                <h4 className="text-center">||||</h4>
                <Formik
                    initialValues={{
                        textA: '',
                        textB: '',
                    }
                    }
                    onSubmit={onSubmit}
                    validationSchema={Yup.object().shape({
                        textA: Yup.number().required('This field is required'),
                        textB: Yup.number().required('This field is required'),
                    })}
                >
                    {({ isValid, resetForm }) => {
                        return (
                            <Form autoComplete="off">
                                <FastField
                                    className="form-control"
                                    type="text"
                                    name="textA"
                                    placeholder="Enter to number A"
                                // error={errors.textA}
                                />
                                {/* <ErrorMessage name={values.textA} component={FormFeedback}/> */}

                                <div style={{ textAlign: 'center', fontSize: '23px' }}><strong>+</strong></div>

                                <FastField
                                    className="form-control"
                                    type="text"
                                    name="textB"
                                    placeholder="Enter to number B"
                                />
                                <br />
                                <Row>
                                    <Col xs={4} />
                                    <Col xs={'auto'}>
                                        <Button
                                            type="submit"
                                            outline={isValid}
                                            color={!isValid ? 'dark' : 'success'}
                                            style={{ width: '22rem' }}
                                            disabled={!isValid}
                                        >
                                            Result
                                              </Button>
                                    </Col>
                                    <Col xs={4} />
                                </Row>
                                <br />
                                <Row>
                                    <Col xs={4} />
                                    <Col xs={'auto'}>
                                        <Button
                                            type="button"
                                            outline
                                            color="danger"
                                            onClick={
                                                () => {
                                                    removeCookie('result');
                                                    setResultCookie(0);
                                                    resetForm()
                                                }
                                            }
                                            style={{ width: '22rem' }}
                                        >
                                            Remove Cookie
                                        </Button>
                                    </Col>
                                    <Col xs={4} />
                                </Row>
                            </Form>
                        )
                    }}
                </Formik>
                <br />
                <h3>Result in Cookie :  {resultCookie}</h3>
            </Container>
        </Fragment>


    );
}

export default RegistrationList;