import EditIcon from '@material-ui/icons/Edit';
import { useState, useEffect } from 'react';
import studentApi from './../../../../api/studentApi';
import subjectApi from './../../../../api/subjectApi';
import majorApi from './../../../../api/majorApi';
import teacherApi from './../../../../api/teacherApi';


const RegisterItem = (props) => {
    const { register, index, onEditClick } = props;
    const [student, setStudent] = useState({});
    const [subject, setSubject] = useState({});
    const [major, setMajor] = useState({});
    const [teacher, setTeacher] = useState({});

    const handleOnEditClick = (event) => {
        event.preventDefault();
        if (onEditClick) onEditClick(register.studentId);
    }

    const loadStudent = () => {
        studentApi.getId(register.studentId).then(stu => {
           if(stu.status === 200) setStudent(stu.data);
        })
    }

    const loadSubject = () => {
        subjectApi.getId(register.subjectId).then(sub => {
            if(sub.status === 200) setSubject(sub.data);
            majorApi.getId(sub.data.majorId).then(maj => {
                if(maj.status === 200) setMajor(maj.data);
            });
            teacherApi.getDetail(sub.data.teacherId).then(tea => {
                if(tea.status === 200) setTeacher(tea.data);
            });
        })
    }

    useEffect(() => {
        loadStudent();
        loadSubject();
    },[register]);


    return (
        <tr>
            <th>{index + 1}</th>
            <td>{student.code}</td>
            <td>{student.lastName} {student.firstName}</td>
            <td>{student.email}</td>
            <td>{subject.name}</td>
            <td>{major.name}</td>
            <td>{teacher.lastName} {teacher.firstName}</td>
            <td>
                <a onClick={handleOnEditClick}><i className="fas fa-edit" style={{color: 'green', cursor: 'pointer'}}></i></a>
            </td>
        </tr>
    );
}

export default RegisterItem;