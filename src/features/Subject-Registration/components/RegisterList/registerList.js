import { Fragment } from "react";
import { Table } from 'reactstrap';
import { makeStyles } from '@material-ui/core';
import RegisterItem from './../RegisterItem/registerItem';


const useStyles = makeStyles((theme) => ({
    text: {
        textAlign: "center",
    }
}));

const RegisterList = (props) => {
    const { registers, onEditClick } = props;
    const classes = useStyles();

    return (
        <Fragment>
            <Table size="sm" striped bordered className={classes.text}>
                <thead>
                    <tr style={{ backgroundColor: '#DAA520' }}>
                        <th colSpan={1} style={{ width: '17px' }}>Stt.</th>
                        <th colSpan={3}>Sinh viên</th>
                        <th colSpan={1}>Môn học</th>
                        <th colSpan={1}>Chuyên ngành</th>
                        <th colSpan={1}>Giảng viên</th>
                        <th colSpan={1}>Chỉnh sửa</th>
                    </tr>
                    <tr style={{ color: "darkblue", backgroundColor: '#FFF5EE' }}>
                        <th>#</th>
                        <th style={{ width: '8rem' }}>MSSV</th>
                        <th>Họ và tên</th>
                        <th>Email</th>
                        <th>Số môn</th>
                        <th>Tên ngành</th>
                        <th>Họ và tên</th>
                        <th>Sửa</th>
                    </tr>
                </thead>
                <tbody>
                    {registers.map((register, index) => (
                        <RegisterItem
                            register={register}
                            index={index}
                            onEditClick={onEditClick}
                        />
                    ))}
                </tbody>
            </Table>
        </Fragment>
    );
}

export default RegisterList;