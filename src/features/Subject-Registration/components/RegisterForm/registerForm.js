import { Button, Grid } from "@material-ui/core";
import { FastField, Form, Formik, Field, FieldArray } from "formik";
import InputField from "../../../../common/Input/inputField";
import { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CheckboxField from './../../../../common/Checkbox/checkboxField';
import { Table } from 'reactstrap';
import Moment from 'moment';
import { useEffect } from 'react';


const useStyles = makeStyles((theme) => ({

    li: {
        marginBottom: '7px',
    },
    th: {
        position: "sticky",
        top: 0,
        background: '#eee'
    }
}));
const RegisterForm = (props) => {

    // const subjects = [
    //     { id: 1, name: 'Web', majorId: 1, teacherId: 2, status: 'Active' },
    //     { id: 2, name: 'App', majorId: 1, teacherId: 2, status: 'Active' },
    //     { id: 3, name: 'Mobile', majorId: 1, teacherId: 2, status: 'Active' },
    //     { id: 4, name: 'Marketing', majorId: 1, teacherId: 2, status: 'Active' },
    // ]
    const classes = useStyles();
    const { initValues, onSubmit, action, subjects } = props;
    const [subs, setSubs] = useState([]);

    // const submit = (value) => {
    //     alert(JSON.stringify(value));
    // }

    useEffect(() => {
        setSubs(subjects);
    }, [subjects]);
    return (
        <Formik
            initialValues={initValues}
            onSubmit={onSubmit}
            enableReinitialize={true}
        >
            {({ values, errors, isValid, isSubmitting }) => {
                return (
                    <Form autoComplete="off">
                        <Grid container spacing={3}>
                            <Grid item xs={12} style={{ marginBottom: '0px', paddingBottom: '0px' }}>
                                <h5 style={{ textAlign: 'center' }}>Thông tin Sinh Viên</h5>
                                <div style={{
                                    marginLeft: '1px',
                                    fontSize: '14px',
                                    letterSpacing: '2px'
                                }}>
                                    <ul>
                                        <li className={classes.li}><strong>Mã sinh viên : </strong>{values.code}</li>
                                        <li className={classes.li}><strong>Họ và tên : </strong>{values.lastName} {values.firstName}</li>
                                        <li className={classes.li}><strong>Gmail : </strong>{values.email}</li>
                                        <li className={classes.li}><strong>Chuyên ngành : </strong>{values.majorName}</li>
                                        <li className={classes.li}><strong>Ngày sinh : </strong>{Moment(values.dob).format("DD-MM-YYYY")}</li>
                                        <li className={classes.li}><strong>Số điện thoại : </strong>{values.phone}</li>
                                    </ul>
                                </div>

                            </Grid>
                            <hr />
                            <Grid item xs={12} style={{ marginTop: '0px', paddingTop: '0px' }}>
                                <h5 style={{ textAlign: 'center', marginBottom: '25px' }}>Đăng ký học phần</h5>
                                <FieldArray
                                    name="subjectIds"
                                    // ({ move, swap, push, insert, unshift, pop }) === arrayHelpers 
                                    render={arrayHelpers => (
                                        <div style={{ overflowY: 'auto', height: '250px' }}>
                                            <Table size={'sm'} style={{
                                                borderCollapse: 'collapse',
                                                width: '100%',
                                            }}>
                                                <thead>
                                                    <tr>
                                                        <th className={classes.th}></th>
                                                        <th className={classes.th}>Tên môn học</th>
                                                        <th className={classes.th}>Chuyên ngành</th>
                                                        <th className={classes.th}>Giảng viên</th>
                                                        <th className={classes.th}>Trạng thái</th>
                                                    </tr>

                                                </thead>
                                                <tbody>
                                                    {subs.map(subject => (
                                                        <Field
                                                            key={subject.id}
                                                            component={CheckboxField}
                                                            name="subjectIds"
                                                            value={subject.id}
                                                            label={subject.name}
                                                            object={subject}
                                                            checked={values.subjectIds.includes(subject.id)}
                                                            onChange={e => {
                                                                if (e.target.checked) arrayHelpers.push(subject.id);
                                                                else {
                                                                    const idx = values.subjectIds.indexOf(subject.id);
                                                                    arrayHelpers.remove(idx);
                                                                }
                                                            }}
                                                        />

                                                    ))}
                                                </tbody>
                                            </Table>
                                        </div>
                                    )}
                                />
                            </Grid>
                        </Grid>
                        <div style={{ float: 'right' }}>
                            <Button type="submit" className="mr-2" variant="contained" color="primary">{action ? 'Đăng Ký' : 'Cập nhật'}</Button>

                        </div>
                    </Form>
                )
            }}
        </Formik >
    );
}

export default RegisterForm;