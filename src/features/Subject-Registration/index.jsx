import { Route, Switch, useRouteMatch } from 'react-router-dom';
import MainPage from './pages/MainPage/index';


const SubjectRegistration = () => {
    const match = useRouteMatch();
    return ( 
        <Switch>
            <Route path={match.url} component={MainPage}/>
        </Switch>
     );
}
 
export default SubjectRegistration;