import { Grid } from '@material-ui/core';
import { Fragment, useState, useEffect } from 'react';
import RegisterList from './../../components/RegisterList/registerList';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import { makeStyles } from '@material-ui/core';
import { Col, Modal, ModalBody, ModalHeader, Row } from "reactstrap";
import RegisterForm from './../../components/RegisterForm/registerForm';
import SearchForm from './../../../form/SearchForm';
import studentApi from './../../../../api/studentApi';
import majorApi from './../../../../api/majorApi';
import { toast } from 'react-toastify';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import subjectApi from './../../../../api/subjectApi';
import registerApi from './../../../../api/registrationApi';
import { ERR_STUDENT_NOT_FOUND, ERR_SUBJECT_NOT_FOUND } from '../../../../config/APIStatus';


const useStyles = makeStyles((theme) => ({
    container: {
        display: 'grid',
        gridGap: theme.spacing(3),

    },
    buttonAdd: {
        float: "right",
        color: "#CC0000",
        marginTop: "10px",
        marginBottom: "10px",
        fontSize: '37px'

    }
}));

const MainPage = () => {
    const classes = useStyles();
    const [status, setStatus] = useState('');
    const [isOpen, setIsOpen] = useState(false);
    const [action, setAction] = useState(false);
    const [btnBack, setBack] = useState(false);
    const [check, setCheck] = useState(false);
    const [changeRegister, setChangeRegister] = useState(false);
    const [student, setStudent] = useState({});
    const [subjects, setSubjects] = useState([]);
    const [registers, setRegisters] = useState([]);
    const [initValuesSearch, setInitValuesSearch] = useState({ searchKey: '' });
    const [initValues, setInitValues] = useState({
        subjectIds: [],
        code: '',
        firstName: '',
        lastName: '',
        dob: '',
        email: '',
        phone: '',
        majorName: '',
        id: ''
    });

    const showModal = () => {
        setStatus('add');
        setIsOpen(true);
        setAction(true);
        setBack(false);
        setCheck(false);

    }

    const hideModal = () => {
        setIsOpen(false);
        setInitValues({
            subjectIds: [],
            code: '',
            firstName: '',
            lastName: '',
            dob: '',
            email: '',
            phone: '',
            majorName: '',
            id: ''
        });
        // setSubjects([]);
    }

    const loadRegister = () => {
        registerApi.getAll().then(res => {
            setRegisters(res.data);
        });
    }

    useEffect(() => {
        loadRegister();
    }, [changeRegister]);

    const backSearch = () => {
        setCheck(false);
        setInitValuesSearch({ searchKey: '' });
        setBack(false);
        setSubjects([]);
    }

    const submitSearchCode = (data) => {
        const params = { code: data.searchKey };
        studentApi.getCode(params).then(res => {
            if (res.status === 200) {
                // setStudent(res.data);
                setCheck(true);
                setBack(true);
                majorApi.getId(res.data.majorId).then(major => {
                    if (major.status === 200) {
                        getSubjectByStudentId(res.data.id);
                        setInitValues({
                            id: res.data.id,
                            code: res.data.code,
                            firstName: res.data.firstName,
                            lastName: res.data.lastName,
                            phone: res.data.phone,
                            email: res.data.email,
                            majorName: major.data.name,
                            subjectIds: [],
                            dob: res.data.dob
                        });
                    }
                })
            }
            else {
                toast.error(ERR_STUDENT_NOT_FOUND.message);
            }
        });
    }

    const getSubjectByStudentId = (studentId) => {
        subjectApi.getSubjectsOfMajorName({ student_id: studentId }).then(sub => {
            setSubjects(sub.data);
        });
    }

    const submit = (value) => {
        // console.log(value);
        if (status === 'add') {
            registerApi.add({ studentId: value.id, subjectIds: value.subjectIds }).then(resAdd => {
                if (resAdd.status === 200) {
                    setChangeRegister(!changeRegister);
                    toast.success('✔️ Lưu thành công rồi nha 😀');
                }
                else toast.error('❗ Lưu thất bại rồi ☹️');
            })
        }
        else {
            registerApi.update({ studentId: value.id, subjectIds: value.subjectIds }).then(resUp => {
                if (resUp.status === 200) {
                    setChangeRegister(!changeRegister);
                    toast.success('✔️ Cập nhật thành công  rồi nha 😀');
                }
                else toast.error('❗ Cập nhật thất bại rồi ☹️');
            })
        }
        hideModal();
    }
    const onEditClick = (studentId) => {
        setStatus('edit');
        const temp = [];
        setBack(false);
        getSubjectByStudentId(studentId);
        const params = { student_id: studentId };
        setCheck(true);
        setIsOpen(true);
        setAction(false);
        registerApi.getRegisterByStudentId(params).then(res => {
            res.data.subjects.map(sub => temp.push(sub.id));
            setInitValues({ ...res.data.student, subjectIds: temp });
        })
    }

    return (
        <Fragment>
            <div style={{ marginRight: '30px', marginLeft: '30px' }}>
                <Grid container={true} justify="space-between" spacing={3}>
                    <Grid item xs={12}>
                        <a onClick={showModal} style={{ cursor: 'pointer' }}><AddCircleOutlineIcon className={classes.buttonAdd} /></a>

                    </Grid>
                </Grid>
                <RegisterList registers={registers} onEditClick={onEditClick} />
            </div>

            <Modal size={'lg'} isOpen={isOpen}>
                <ModalHeader toggle={hideModal}>
                    {btnBack && <a onClick={backSearch}><ArrowBackIcon className="mb-1" /></a>} {action ? ' Đăng ký môn học' : 'Thay đổi thông tin đăng ký'}
                </ModalHeader>
                <ModalBody>
                    {isOpen &&
                        <div>
                            <Row>
                                <Col sm="12" md={{ size: 8, offset: 3 }}>
                                    {!check &&
                                        <SearchForm initValues={initValuesSearch} onSubmit={submitSearchCode} placeholder={'Nhập vào mã số sinh viên...'} />
                                    }
                                </Col>
                            </Row>

                            {check &&
                                <RegisterForm
                                    initValues={initValues}
                                    subjects={subjects}
                                    onSubmit={submit}
                                    action={action}
                                />
                            }

                        </div>

                    }

                </ModalBody>
            </Modal>
        </Fragment>

    );
}

export default MainPage;