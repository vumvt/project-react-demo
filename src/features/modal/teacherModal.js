import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import TeacherForm from "../Teacher/components/TeacherForm/TeacherForm";

const TeacherModal = (props) => {
    const { isOpen, toggle, className } = props;
    return (

        <Modal isOpen={isOpen} toggle={toggle} className={className} backdrop={'static'}>
            <ModalHeader toggle={toggle}>Modal title</ModalHeader>
            <ModalBody>
                <TeacherForm />
            </ModalBody>
            <ModalFooter>
                <Button color="primary" onClick={toggle}>Save Change</Button>
                <Button color="secondary" onClick={toggle}>Cancel</Button>
            </ModalFooter>
        </Modal>

    );
}

export default TeacherModal;