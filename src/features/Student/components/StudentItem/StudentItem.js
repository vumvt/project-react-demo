import { useEffect, useState } from "react";
import majorApi from "../../../../api/majorApi";
import Moment from 'moment';
import EditIcon from '@material-ui/icons/Edit';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
const StudentItem = (props) => {
    const { student, handleOnEditStudent, size, currentPage, handleOnRemoveStudent, index } = props;
    const [gender, setGender] = useState('');
    const [major, setMajor] = useState('');


    useEffect(() => {
        const fetchMajorObject = async () => {
            try {
                const majorObject = await majorApi.getId(student.majorId);
                setMajor(majorObject.data);
            } catch (error) {
                console.log('Error get major id : ', error);
            }
        }
        fetchMajorObject();

        if (student.gender === 0) setGender('Nam')
        else if (student.gender === 1) setGender('Nữ')
        else setGender('Khác')
    }, [])

    const handleOnEdit = (event) => {
        event.preventDefault();
        handleOnEditStudent && handleOnEditStudent(student.id);
    }
    const handleOnRemove = (event) => {
        event.preventDefault();
        handleOnRemoveStudent && handleOnRemoveStudent(student.id);
    }



    return (
        <tr>
            <th>{size * (currentPage - 1) + index + 1}</th>
            <td>{student.lastName} {student.firstName}</td>
            <td>{student.code}</td>
            <td>{student.email}</td>
            <td>{gender}</td>
            <td>{Moment(student.dob).format("DD-MM-YYYY")}</td>
            <td>{major.name}</td>
            <td>
                <a onClick={handleOnEdit}><i className="fas fa-edit" style={{ color: 'green', cursor: 'pointer' }}></i></a>
            </td>
            <td>
                <a onClick={handleOnRemove}><DeleteForeverIcon style={{ color: '#B22222', cursor: 'pointer' }} /></a>
            </td>
        </tr>
    );
}

export default StudentItem;