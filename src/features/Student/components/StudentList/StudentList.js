import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core';
import { Table } from 'reactstrap';
import StudentItem from './../StudentItem/StudentItem';

const useStyles = makeStyles((theme) => ({
    container: {
        display: 'grid',
        gridGap: theme.spacing(3),

    },
    buttonAdd: {
        float: "right",
        color: "#CC0000",
        marginTop: "10px",
        marginBottom: "10px",
        fontSize: '37px'

    },
    text: {
        textAlign: "center"
    },
    divider: {
        margin: theme.spacing(2, 0),
    },
}));
const StudentList = (props) => {
    const classes = useStyles();
    const { students, handleOnRemoveStudent, size, handleOnEditStudent, currentPage } = props;

    return (

        <Table striped bordered className={classes.text} size={'sm'}>
            <thead style={{ backgroundColor: '#DAA520' }}>
                <tr>
                    <th>Stt.</th>
                    <th>Họ và tên</th>
                    <th>MSSV</th>
                    <th>Gmail</th>
                    <th>Giới tính</th>
                    <th>Ngày sinh</th>
                    <th>Chuyên ngành</th>
                    <th>Chỉnh sửa</th>
                    <th>Xóa</th>
                </tr>
            </thead>
            <tbody>
                {students.map((student, index) => (
                    <StudentItem
                        index={index}
                        key={student.id}
                        currentPage={currentPage}
                        size={size}
                        student={student}
                        handleOnEditStudent={handleOnEditStudent}
                        handleOnRemoveStudent={handleOnRemoveStudent}
                    />
                ))}
            </tbody>
        </Table>
    );
}

export default StudentList;