import { Button, FormLabel, Grid } from "@material-ui/core";
import { FastField, Field, Form, Formik } from "formik";
import { Fragment } from "react";
import SaveIcon from '@material-ui/icons/Save';
import RotateLeftIcon from '@material-ui/icons/RotateLeft';


import * as Yup from "yup";
import DateTimeField from './../../../../common/Date/DateTimeField';
import InputField from './../../../../common/Input/inputField';
import RadioField from './../../../../common/Radio/radioField';
import SelectField from './../../../../common/Select/selectField';

const StudentForm = (props) => {
    const { initValues, onSubmit, majors, action } = props;

    // const initValues = {
    //     lastName: '',
    //     firstName: '',
    //     code: '',
    //     email: '',
    //     gender: 'Male',
    //     major: null,
    //     dob: null,
    //     phone: ''
    // }

    //format phone
    const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/

    const validateSchema = Yup.object().shape({
        dob: Yup.string().required('Không được bỏ trống !').nullable(),
        lastName: Yup.string().required('Không được bỏ trống !'),
        firstName: Yup.string().required('Không được bỏ trống !'),
        code: Yup.string().required('Không được bỏ trống !'),
        email: Yup.string().email('Email không hợp lệ !').required('Không được bỏ trống !'),
        gender: Yup.number().required('Không được bỏ trống !'),
        phone: Yup.string().required('Không được bỏ trống !').max(10, 'Số điện thoại không hợp lệ !').matches(phoneRegExp, 'Số điện thoại không hợp lệ !'),
        majorId: Yup.number().required('Không được bỏ trống !').nullable(),
        age: Yup.number()
            .required('Không được bỏ trống !')
            .positive('Không được nhỏ hơn 1')
            .integer('Tuổi phải là số nguyên dương'),
    });
    
    return (
        <Formik
            initialValues={initValues}
            onSubmit={onSubmit}
            validationSchema={validateSchema}
            enableReinitialize={true}
        >
            {({ values, isValid, resetForm }) => {
                return (
                    <Fragment>
                        <Form autoComplete="off">
                            <Grid container={true} spacing={3}>
                                <Grid item={true} xs={6}>
                                    <FastField
                                        component={InputField}
                                        name="lastName"

                                        label="Họ *"
                                        placeholder="Điện họ của sinh viên !"
                                        fullWidth={true}
                                    />
                                </Grid>
                                <Grid item={true} xs={6}>
                                    <FastField
                                        component={InputField}
                                        name="firstName"

                                        label="Tên *"
                                        placeholder="Điền tên của sinh viên !"
                                        fullWidth={true}
                                    />
                                </Grid>
                                <Grid item={true} xs={8}>
                                    <FastField
                                        component={InputField}
                                        name="code"

                                        label="Mã số sinh viên *"
                                        placeholder="Điền mã sinh viên !"
                                        fullWidth={true}
                                    />
                                </Grid>
                                <Grid item={true} xs={4}>
                                    <FastField
                                        component={InputField}
                                        name="age"

                                        label="Tuổi *"
                                        type="number"
                                        placeholder=""
                                        fullWidth={true}
                                    />
                                </Grid>
                                <Grid item={true} xs={12}>
                                    <FastField
                                        component={InputField}
                                        name="email"

                                        label="Email *"
                                        placeholder="example@gmail.com"
                                        fullWidth={true}
                                    />
                                </Grid>
                                <Grid item={true} xs={6}>
                                    <FastField
                                        component={DateTimeField}
                                        name="dob"

                                        label="Ngày sinh *"
                                        placeholder=""
                                        format="dd/MM/yyyy"
                                        fullWidth={true}
                                        placeholder="01/12/2020"
                                    />
                                </Grid>
                                <Grid item={true} xs={6}>
                                    <FastField
                                        component={InputField}
                                        name="phone"

                                        label="Số điện thoại *"
                                        placeholder="000-000-0000"
                                        fullWidth={true}
                                    />
                                </Grid>
                                <Grid item={true} xs={7}>
                                <FormLabel component="legend" style={{ fontSize: '15px' }}>Giới tính *</FormLabel>
                                    <Field
                                        component={RadioField}
                                        name="gender"

                                        checked={values.gender === 0}
                                        value='0'
                                        label="Nam"
                                    />
                                    <Field
                                        component={RadioField}
                                        name="gender"

                                        checked={values.gender === 1}
                                        value='1'
                                        label="Nữ"
                                    />
                                    <Field
                                        component={RadioField}
                                        name="gender"

                                        checked={values.gender === 2}
                                        value='2'
                                        label="Khác"
                                    />
                                </Grid>
                                <Grid item={true} xs={5}>
                                <FastField
                                        component={SelectField}
                                        name="majorId"

                                        options={majors}
                                        check={true}
                                        label="Chuyên ngành *"
                                    />
                                </Grid>

                                <hr />

                                <Grid item={true} xs={12}>
                                    <Button
                                        variant="contained"
                                        color="secondary"
                                        size="small"
                                        // className="mr-2"
                                        startIcon={<RotateLeftIcon />}
                                        onClick={resetForm}
                                    >
                                        Reset
                                    </Button>
                                    <div className="float-right">
                                        <Button
                                            variant="contained"
                                            color="primary"
                                            size="small"
                                            type="submit"
                                            disabled={!isValid}
                                            className="mr-2"
                                            startIcon={<SaveIcon />}
                                        >
                                            {action ? 'Lưu' : 'Cập nhật'}
                                        </Button>
                                    </div>

                                </Grid>
                            </Grid>
                        </Form>
                    </Fragment>
                )
            }}
        </Formik>
    );
}

export default StudentForm;