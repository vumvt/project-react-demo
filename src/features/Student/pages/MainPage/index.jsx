import { Container, Grid, makeStyles } from '@material-ui/core';
import React, { Fragment, useEffect, useState } from 'react'
import { Modal, ModalBody, ModalHeader } from 'reactstrap';
import StudentList from '../../components/StudentList/StudentList';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import StudentForm from '../../components/StudentForm/StudentForm';
import majorApi from './../../../../api/majorApi';
import studentApi from '../../../../api/studentApi';
import { toast } from 'react-toastify';
import Pagination from "react-pagination-library";
import {
    ERR_ADD,
    ERR_DELETE,
    ERR_UPDATE,
    SUCCESS_ADD,
    SUCCESS_DELETE,
    SUCCESS_UPDATE,
    ERR_STUDENT_IS_LEARNING
} from './../../../../config/APIStatus';

const useStyles = makeStyles((theme) => ({
    container: {
        display: 'grid',
        gridGap: theme.spacing(3),

    },
    buttonAdd: {
        float: "right",
        color: "#CC0000",
        marginTop: "10px",
        marginBottom: "10px",
        fontSize: '37px'

    },
}));

const MainPage = () => {
    const [students, setStudents] = useState([]);
    const [isOpen, setIsOpen] = useState(false);
    const classes = useStyles();
    const [majors, setMajors] = useState([]);
    const [reloadList, setReloadList] = useState(false);
    const [action, setAction] = useState(false);
    const [selectedStudent, setSelectedStudent] = useState(
        {
            lastName: '',
            firstName: '',
            code: '',
            email: '',
            age: '',
            gender: 0,
            majorId: '',
            dob: '',
            phone: ''
        }
    );
    const [currentPage, setCurrentPage] = useState(1);
    const [totalEl, setTotalEl] = useState();
    const [size, setSize] = useState(5);


    const loadingStudent = () => {
        const params = { page: currentPage, size: size };
        studentApi.getAll(params).then(res => {
            if (res.status === 200) {
                setStudents(res.data.content);
                setTotalEl(res.data.totalElement);
            }
        }).catch(err => console.log(err));
    }

    const loadingMajor = async () => {
        try {
            const majorsResponse = await majorApi.getAll();
            setMajors(majorsResponse.data);
        } catch (error) {
            console.log('Error when get all majors response: ', error);
        }
    }

    useEffect(() => {
        loadingStudent();
    }, [reloadList]);

    useEffect(() => {
        loadingMajor();
    }, []);


    const handleOnEditStudent = async (id) => {
        setIsOpen(true);
        setAction(false);
        try {
            const studentResponse = await studentApi.getId(id);
            setSelectedStudent({ ...studentResponse.data });
        } catch (error) {
            console.log('Error when get id student update: ', error);
        }
    }

    const handleOnRemoveStudent = (id) => {
        studentApi.delete(id).then(res => {
            if (res.status === 200) {
                toast.success(SUCCESS_DELETE.message);
                setReloadList(!reloadList);
            } else {
                toast.error(ERR_STUDENT_IS_LEARNING.message);
            }
        });
    }
    // pagination ui
    const changeCurrentPage = numPage => {
        setCurrentPage(numPage);
        setReloadList(!reloadList);
    };


    const showModal = () => {
        setIsOpen(true);
        setAction(true);
    }

    const hideModal = () => {
        setIsOpen(false);
        setSelectedStudent({
            lastName: '',
            firstName: '',
            code: '',
            email: '',
            age: 0,
            gender: 0,
            majorId: '',
            dob: '',
            phone: ''
        });
    }

    const onSubmit = async (student) => {
        if (!student.id) {
            studentApi.addNew(student).then(res => {
                if (res.status === 200) {
                    setReloadList(!reloadList);
                    toast.success(SUCCESS_ADD.message);
                } else {
                    toast.error(ERR_ADD.message);
                }
            });
        } else {
            studentApi.update(student).then(res => {
                if (res.status === 200) {
                    setReloadList(!reloadList);
                    toast.success(SUCCESS_UPDATE.message);
                } else {
                    toast.error(ERR_UPDATE.message);
                }
            });
        }
        hideModal();
    }

    return (
        <Fragment>
            <div style={{ marginRight: '30px', marginLeft: '30px' }}>
                <Grid item xs={12}>
                    <a onClick={showModal} style={{ cursor: 'pointer' }}>
                        <AddCircleOutlineIcon className={classes.buttonAdd} />
                    </a>
                </Grid>
                <StudentList
                    students={students}
                    currentPage={currentPage}
                    size={size}
                    handleOnRemoveStudent={handleOnRemoveStudent}
                    handleOnEditStudent={handleOnEditStudent}
                />
                <div
                    style={
                        {
                            fontSize: '13px',
                            color: 'red',
                            float: 'right'
                        }
                    }
                >
                    <Pagination
                        currentPage={currentPage}
                        totalPages={(totalEl / size)}
                        changeCurrentPage={changeCurrentPage}
                        theme="square-fill"
                    />
                </div>
            </div>

            <Modal size={'md'} isOpen={isOpen} backdrop="static">
                <ModalHeader toggle={hideModal}>
                    {action ? ' Thêm mới sinh viên' : 'Thay đổi thông tin sinh viên'}
                </ModalHeader>
                <ModalBody>
                    {isOpen &&
                        <StudentForm
                            initValues={selectedStudent}
                            onSubmit={onSubmit}
                            majors={majors}
                            action={action}
                        />}

                </ModalBody>
            </Modal>
        </Fragment>
    );
}

export default MainPage;