import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router-dom';
import NotFound from './../../page/notFound';
import StudentForm from './components/StudentForm/StudentForm';
import MainPage from './pages/MainPage';

const Student = () => {
    const match = useRouteMatch();
    return ( 
        <Switch>
        <Route exact path={match.url} component={MainPage} />
  
        <Route path={`${match.url}/add`} component={StudentForm} />
  
        <Route component={NotFound} />
      </Switch>
     );
}
 
export default Student;