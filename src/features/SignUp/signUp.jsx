import { Container } from '@material-ui/core';
import React from 'react';
import { Row, Col } from 'reactstrap';
import SignUpForm from './SignUpForm';
import UserApi from './../../api/userApi';
import { useHistory } from 'react-router-dom';
import { toast } from 'react-toastify';

const SignUp = () => {
    const history = useHistory();

    const submit = (value) => {
        UserApi.signUp(value).then(res => {
            if (res.id !== null) {
                history.push('/login');
                toast.success('✔️ Đăng ký tài khoản thành công 😀');
            }
        }).catch(err => {
            toast.error('❗ Đăng ký thất bại rồi ☹️');
            console.log(err);
        });
    }
    
    return (
        <Container>
            <Row style={{ alignItems: 'center', marginTop: '2rem' }}>
                <Col  sm="12" md={{ size: 5, offset: 3 }} style={{ alignSelf: 'center' }}>
                    <SignUpForm submit={submit} />
                </Col>
            </Row>
        </Container>
    );
}

export default SignUp;