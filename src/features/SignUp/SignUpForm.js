import { Card, CardActions, CardContent, IconButton, InputAdornment } from '@material-ui/core';
import { FastField, Form, Formik } from 'formik';
import React, { useState } from 'react';
import { CardBody} from 'reactstrap';
import InputField from '../../common/Input/inputField';
import { Button } from '@material-ui/core';
import * as Yup from 'yup';
import { Visibility, VisibilityOff } from '@material-ui/icons';


const SignUpForm = (props) => {
    const {submit} = props;
    const [showPassword, setShowPassword] = useState(false);
    const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/

    const validateSchema = Yup.object().shape({
        username: Yup.string().required('Không được bỏ trống !').matches(
            /^[a-zA-Z0-9]+$/,
            'Tên tài khoản không được chứa ký tự đặc biệt hoặc khoảng trắng'
          ),
        phone: Yup.string().required('Không được bỏ trống !').max(10, 'Số điện thoại không hợp lệ !').matches(phoneRegExp, 'Phone is valid !'),
        email: Yup.string().email('Email không hợp lệ !').required('Không được bỏ trống !'),
        password: Yup.string().required('Không được bỏ trống !'),
        confirm_password: Yup.string().oneOf([Yup.ref('password')], "Mật khẩu không giống !")
            .required('Không được bỏ trống !')
    })
    return (
        <Formik
            initialValues={{
                username: '',
                password: '',
                phone: '',
                email: '',
                confirm_password: ''
            }}
            onSubmit={submit}
            validationSchema={validateSchema}
        >
            {({ resetForm, isValid }) => {
                return (
                    <Card style={
                        {
                            borderRadius: '5px',
                            borderRight: '4px blue inset',
                            borderBottom: '4px blue outset',
                            borderLeft: '1px blue outset'
                        }
                    }>
                        <CardContent style={{ backgroundColor: '#0080ff' }}>
                            <h2 style={{ textAlign: 'center' }}>Đăng ký</h2>
                        </CardContent>
                        <Form autoComplete="off">
                            <CardBody>

                                <FastField
                                    component={InputField}
                                    name="username"

                                    fullWidth={true}
                                    label="Tài khoản *"
                                    placeholder="Điền tên tài khoản !"
                                />
                                <br />
                                <FastField
                                    component={InputField}
                                    name="email"

                                    fullWidth={true}
                                    label="Email *"
                                    placeholder="example@gmail.com"
                                    type="email"

                                />
                                <br />
                                <FastField
                                    component={InputField}
                                    name="phone"

                                    fullWidth={true}
                                    label="Số điện thoại *"
                                    placeholder="000-000-0000"

                                />
                                <br />
                                <FastField
                                    component={InputField}
                                    name="password"

                                    fullWidth={true}
                                    label="Mật khẩu *"
                                    placeholder="Điền mật khẩu của bạn !"
                                    type="password"
                                    InputProps={{
                                        endAdornment: (
                                            <InputAdornment position="end">
                                                <IconButton
                                                    aria-label="toggle password visibility"
                                                    onClick={() => {
                                                        setShowPassword(!showPassword)
                                                        console.log(showPassword)
                                                    }}
                                                    onMouseDown={() => {
                                                        setShowPassword(!showPassword)
                                                        console.log(showPassword)
                                                    }}
                                                    edge="end"
                                                >
                                                    {showPassword ? <Visibility /> : <VisibilityOff />}
                                                </IconButton>
                                            </InputAdornment>

                                        ),
                                    }}

                                />
                                <br />
                                <FastField
                                    component={InputField}
                                    name="confirm_password"

                                    fullWidth={true}
                                    label="Xác nhận mật khẩu *"
                                    type="password"
                                    InputProps={{
                                        endAdornment: (
                                            <InputAdornment position="end">
                                                <IconButton
                                                    aria-label="toggle password visibility"
                                                    onClick={() => {
                                                        setShowPassword(!showPassword)
                                                        console.log(showPassword)
                                                    }}
                                                    onMouseDown={() => {
                                                        setShowPassword(!showPassword)
                                                        console.log(showPassword)
                                                    }}
                                                    edge="end"
                                                >
                                                    {showPassword ? <Visibility /> : <VisibilityOff />}
                                                </IconButton>
                                            </InputAdornment>

                                        ),
                                    }}

                                />

                            </CardBody>
                            <CardActions style={{ float: 'right', marginBottom: '5px' }}>
                                <Button
                                    size="large"
                                    color="primary"
                                    variant="contained"
                                    size="small"
                                    type="submit"
                                    disabled={!isValid}
                                >
                                    Đăng ký
                                            </Button>
                                <Button
                                    size="small"
                                    style={{ backgroundColor: '#808080' }}
                                    variant="contained"
                                    size="small"
                                    onClick={resetForm}
                                >
                                    Reset
                                            </Button>

                            </CardActions>
                        </Form>
                    </Card>
                )
            }}
        </Formik>
    );
}

export default SignUpForm;