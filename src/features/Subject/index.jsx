
import { Route, useRouteMatch } from 'react-router-dom';
import { Switch } from 'react-router-dom';
import MainPage from './../Subject/pages/index';


const Subject = () => {
    const match = useRouteMatch();
    return ( 
       <Switch>
           <Route path={match.url} component={MainPage}/>
       </Switch>
     );
}
 
export default Subject;