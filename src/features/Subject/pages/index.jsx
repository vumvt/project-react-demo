import { Container, Grid } from "@material-ui/core";
import { Fragment, useState, useEffect } from 'react';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import subjectApi from './../../../api/subjectApi';
import { makeStyles } from '@material-ui/core/styles';
import SubjectList from './../components/SubjectList/SubjectList';
import SearchForm from "../../form/SearchForm";
import { toast } from 'react-toastify';
import { ERR_ADD, ERR_DELETE, ERR_SUBJECT_NOT_FOUND, ERR_UPDATE, SUCCESS_ADD, SUCCESS_DELETE, SUCCESS_UPDATE } from './../../../config/APIStatus';
import { Modal, ModalHeader, ModalBody } from 'reactstrap';
import SubjectForm from './../components/SubjectForm/SubjectForm';
import majorApi from './../../../api/majorApi';
import teacherApi from './../../../api/teacherApi';

const useStyles = makeStyles((theme) => ({
    container: {
        display: 'grid',
        gridGap: theme.spacing(3),

    },
    buttonAdd: {
        float: "right",
        color: "#CC0000",
        marginTop: "10px",
        marginBottom: "10px",
        fontSize: '37px'

    }
}));
const MainPage = () => {
    const [subjectList, setSubjects] = useState([]);
    const [majors, setMajors] = useState([]);
    const [teachers, setTeachers] = useState([]);
    const [isChange, setIsChange] = useState(false);
    const [initValue, setInitValue] = useState(
        {
            code: '',
            name: '',
            majorId: '',
            teacherId: ''
        }
    );
    const [action, setAction] = useState(false);

    const classes = useStyles();
    const [isOpen, setIsOpen] = useState(false);


    const loadSubjects = () => {
        subjectApi.getAll().then(res => {
            if (res.status === 200) {
                setSubjects(res.data);
            }
        }).catch(err => { console.log(err) });
    }

    const loadMajors = () => {
        majorApi.getAll().then(res => {
            if (res.status === 200) {
                setMajors(res.data);
            }
        }).catch(err => console.log(err));
    }

    const loadTeachers = () => {
        teacherApi.getAll().then(res => {
            if (res.status === 200) {
                setTeachers(res.data);
            }
        }).catch(err => { console.log(err) });
    }

    useEffect(() => {
        loadSubjects();
    }, [isChange]);

    useEffect(() => {
        loadTeachers();
        loadMajors();
    }, []);

    const handleOnEdit = (id) => {
        showModal();
        setAction(false);
        subjectApi.getId(id).then(res => {
            setInitValue(res.data);
        })
    }

const handleOnRemove = (id) => {
    subjectApi.delete(id).then(res => {
        if (res.status === 200) {
            setIsChange(!isChange);
            toast.success(SUCCESS_DELETE.message);
        } else {
            toast.error(ERR_DELETE.message);
        }
    })
}

const showModal = () => {
    setIsOpen(true);
    setAction(true);
}
const hideModal = () => {
    setIsOpen(false);
    setInitValue({
        code: '',
        name: '',
        majorId: '',
        teacherId: ''
    });
}

const onSubmit = (subject) => {
    if (!subject.id) {
        subjectApi.add(subject).then(res => {
            if (res.status === 200) {
                setIsChange(!isChange);
                toast.success(SUCCESS_ADD.message);
            }
            else {
                toast.error(ERR_ADD.message);
            }
        });
    }
    else {
        subjectApi.update(subject).then(res => {
            if (res.status === 200) {
                setIsChange(!isChange);
                toast.success(SUCCESS_UPDATE.message);
            } else {
                toast.error(ERR_UPDATE.message);
            }
        })
    }

    hideModal();
}

const onSearch = (data) => {
    const params = { search_key: data.searchKey };
    subjectApi.searchKey(params).then(res => {
        if (res.status === 200) {
            setSubjects(res.data);
        }

        else
            toast.error(`${ERR_SUBJECT_NOT_FOUND.message}`);
    });
}

return (
    <Fragment>
        {/* <Container fixed> */}
        <div style={{marginRight: '30px', marginLeft: '30px'}}>
            <Grid container={true} justify="space-between" spacing={3}>
                <Grid item xs={6}>
                    <SearchForm
                        initValues={{ searchKey: '' }}
                        onSubmit={onSearch}
                        placeholder={'Nhập vào từ khóa tên hoặc mã môn học...'}
                    />
                </Grid>
                <Grid item xs={6}>
                    <a onClick={showModal} style={{ cursor: 'pointer' }}><AddCircleOutlineIcon className={classes.buttonAdd} /></a>

                </Grid>
            </Grid>
            <SubjectList
                subjectList={subjectList}
                handleOnEdit={handleOnEdit}
                handleOnRemove={handleOnRemove}
            />
        {/* </Container> */}
        </div>
        
        <Modal isOpen={isOpen}>
            <ModalHeader toggle={hideModal}>
                {action ? 'Thêm mới môn học' : 'Thay đổi thông tin môn học'}
            </ModalHeader>
            <ModalBody>
                {isOpen &&
                    <SubjectForm
                        initialValues={initValue}
                        onSubmit={onSubmit}
                        action={action}
                        majors={majors}
                        teachers={teachers}
                    />
                }

            </ModalBody>
        </Modal>
    </Fragment>
);
}

export default MainPage;