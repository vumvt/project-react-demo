import { FastField, Formik, Form } from "formik";
import { Fragment } from 'react';
import { Grid, Button } from "@material-ui/core";
import InputField from "../../../../common/Input/inputField";
import * as Yup from 'yup';
import SelectField from './../../../../common/Select/selectField';
import RotateLeftIcon from '@material-ui/icons/RotateLeft';
import SaveIcon from '@material-ui/icons/Save';

const SubjectForm = (props) => {
    const {initialValues , onSubmit, majors, teachers, action} = props;

    const validate = Yup.object().shape({
        code: Yup.string().required('Không được bỏ trống !'),
        name: Yup.string().required('Không được bỏ trống !'),
        majorId: Yup.number().required('Không được bỏ trống !').nullable(),
        teacherId: Yup.number().required('Không được bỏ trống !').nullable(),
    })

    return (
        <Formik
        validationSchema={validate}
        onSubmit={onSubmit}
        initialValues={initialValues}
        enableReinitialize={true}
        >
            {({ values, errors, isValid, resetForm }) => {
                return (
                    <Fragment>
                        <Form autoComplete="off">
                            <Grid container={true} justify="space-between" spacing={3}>
                                <Grid item xs={6}>
                                    <FastField
                                        component={InputField}
                                        name="code"

                                        label="Mã môn học *"
                                        placeholder="Điền mã môn học !"
                                        fullWidth={true}
                                    />
                                </Grid>
                                <Grid item xs={6}>
                                    <FastField
                                        component={InputField}
                                        name="name"

                                        label="Tên môn học *"
                                        placeholder="Điền tên môn học !"
                                        fullWidth={true}
                                    />
                                </Grid>
                                <Grid item={true} xs={6}>
                                <FastField
                                        component={SelectField}
                                        name="majorId"

                                        options={majors}
                                        check={true}
                                        label="Chuyên ngành *"
                                    />
                                </Grid>
                                <Grid item={true} xs={6}>
                                <FastField
                                        component={SelectField}
                                        name="teacherId"

                                        options={teachers}
                                        check={false}
                                        label="Giảng viên *"
                                    />
                                </Grid>
                                <hr />

                                <Grid item xs={12}>
                                    <Button
                                        variant="contained"
                                        color="secondary"
                                        size="small"
                                        // className="mr-2"
                                        startIcon={<RotateLeftIcon />}
                                        onClick={resetForm}
                                    >
                                        Reset
                                    </Button>
                                    <div className="float-right">
                                        <Button
                                            variant="contained"
                                            color="primary"
                                            size="small"
                                            type="submit"
                                            disabled={!isValid}
                                            className="mr-2"
                                            startIcon={<SaveIcon />}
                                        >
                                            {action ? 'Lưu' : 'Cập nhật'}
                                        </Button>
                                    </div>

                                </Grid>

                            </Grid>
                        </Form>
                    </Fragment>

                )
            }}
        </Formik>
    );
}

export default SubjectForm;