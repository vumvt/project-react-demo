import { Button } from 'reactstrap';
import majorApi from './../../../../api/majorApi';
import { useState } from 'react';
import { toast } from 'react-toastify';
import { ERR_MAJOR_NOT_FOUND } from './../../../../config/APIStatus';
import { ERR_TEACHER_NOT_FOUND } from './../../../../config/APIStatus';
import teacherApi from './../../../../api/teacherApi';
import { useEffect } from 'react';
import EditIcon from '@material-ui/icons/Edit';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';


const SubjectItem = (props) => {
    const { subject, index, handleOnEditClick, handleOnRemoveClick } = props;
    const [major, setMajor] = useState({});
    const [teacher, setTeacher] = useState({});

    const getMajor = () => majorApi.getId(subject.majorId).then(res => {
        if (res) {
            setMajor(res.data);
        }
        else {
            toast.error(`${ERR_MAJOR_NOT_FOUND.status}-${ERR_MAJOR_NOT_FOUND.message}`);
        }
    });

    const getTeacher = () => teacherApi.getDetail(subject.teacherId).then(res => {
        if (res) {
            setTeacher(res.data);
        }
        else {
            toast.error(`${ERR_TEACHER_NOT_FOUND.status}-${ERR_TEACHER_NOT_FOUND.message}`);
        }
    })

    useEffect(() => {
        getMajor();
        getTeacher();
    }, []);


    const handleOnEdit = (event) => {
        event.preventDefault();
        handleOnEditClick && handleOnEditClick(subject.id);
    }

    const handleOnRemove = (event) => {
        event.preventDefault();
        handleOnRemoveClick && handleOnRemoveClick(subject.id);
    }

    return (
        <tr>
            <th>{index + 1}</th>
            <td>{subject.code}</td>
            <td>{subject.name}</td>
            <td>{major.name}</td>
            <td>{teacher.lastName} {teacher.firstName}</td>
            <td>{subject.status === 1 && 'Đang mở'}</td>
            <td>
                <a onClick={handleOnEdit}><i className="fas fa-edit" style={{color: 'green', cursor: 'pointer'}}></i></a>
            </td>
            <td>
                <a onClick={handleOnRemove}><DeleteForeverIcon style={{color: '#B22222', cursor: 'pointer'}}/></a>
            </td>
        </tr>
    );
}

export default SubjectItem;