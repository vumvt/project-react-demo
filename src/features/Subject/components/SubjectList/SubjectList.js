import { Fragment } from 'react';
import { Table } from 'reactstrap';
import { makeStyles } from '@material-ui/core';
import SubjectItem from './../SubjectItem/SubjectItem';


const useStyles = makeStyles((theme) => ({
    container: {
        display: 'grid',
        gridGap: theme.spacing(3),

    },
    text: {
        textAlign: "center"
    }
}));

const SubjectList = (props) => {
    const { subjectList, handleOnEdit, handleOnRemove } = props;
    const classes = useStyles();
    return (
        <Fragment>
            <Table striped bordered className={classes.text} size={'sm'}>
                <thead style={{backgroundColor: '#DAA520'}}>
                    <tr>
                        <th>Stt.</th>
                        <th>Mã số</th>
                        <th>Name</th>
                        <th>Chuyên ngành</th>
                        <th>Giảng viên</th>
                        <th>Trạng thái</th>
                        <th>Chỉnh sửa</th>
                        <th>Xóa</th>
                    </tr>
                </thead>
                <tbody>
                    {subjectList.map((subject, index) => (
                        <SubjectItem
                            key={subject.id}
                            subject={subject}
                            index={index}
                            handleOnEditClick={handleOnEdit}
                            handleOnRemoveClick={handleOnRemove}
                        />
                    ))}

                </tbody>
            </Table>
        </Fragment>
    );
}

export default SubjectList;