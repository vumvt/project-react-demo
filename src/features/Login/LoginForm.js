import { Card, CardActions, CardContent, IconButton, InputAdornment } from '@material-ui/core';
import { FastField, Form, Formik } from 'formik';
import React, { useState } from 'react';
import { CardBody } from 'reactstrap';
import InputField from '../../common/Input/inputField';
import { Button } from '@material-ui/core';
import { Visibility, VisibilityOff } from '@material-ui/icons';
import { useHistory } from 'react-router-dom';

const LoginForm = (props) => {
    const { submit } = props;
    const [showPassword, setShowPassword] = useState(false);
    const history = useHistory();
    return (
        <Formik
            initialValues={{
                username: '',
                password: ''
            }}
            onSubmit={submit}
        >
            {({ isValid }) => {
                return (
                    <Card style={
                        {
                            borderRadius: '5px',
                            borderRight: '4px blue inset',
                            borderBottom: '4px blue outset',
                            borderLeft: '1px blue outset'
                        }
                    }>
                        <CardContent style={{ backgroundColor: '#0080ff' }}>
                            <h2 style={{ textAlign: 'center' }}>Đăng nhập</h2>
                        </CardContent>
                        <Form autoComplete="off">
                            <CardBody>

                                <FastField
                                    component={InputField}
                                    name="username"

                                    fullWidth={true}
                                    label="Tài khoản *"
                                />
                                <br />
                                <FastField
                                    component={InputField}
                                    name="password"

                                    fullWidth={true}
                                    label="Mật khẩu *"
                                    type={showPassword ? 'text' : 'password'}
                                    InputProps={{
                                        endAdornment: (
                                            <InputAdornment position="end">
                                                <IconButton
                                                    aria-label="toggle password visibility"
                                                    onClick={() => {
                                                        setShowPassword(!showPassword)
                                                        console.log(showPassword)
                                                    }}
                                                    onMouseDown={() => {
                                                        setShowPassword(false)
                                                        console.log(showPassword)
                                                    }}
                                                    edge="end"
                                                >
                                                    {showPassword ? <Visibility /> : <VisibilityOff />}
                                                </IconButton>
                                            </InputAdornment>

                                        ),
                                    }}

                                />

                            </CardBody>
                            <CardActions style={{ float: 'right', marginBottom: '5px' }}>
                                <Button
                                    size="large"
                                    color="primary"
                                    variant="contained"
                                    size="small"
                                    type="submit"
                                    disabled={!isValid}
                                >
                                    Đăng nhập
                            </Button>
                                <Button
                                    size="small"
                                    variant="contained"
                                    color="primary"
                                    size="small"
                                    onClick={() => history.push('/signUp')}
                                >
                                    Đăng ký
                            </Button>

                            </CardActions>
                        </Form>
                    </Card>
                )
            }}
        </Formik>
    );
}

export default LoginForm;