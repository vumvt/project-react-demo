import { createSlice } from "@reduxjs/toolkit";
import cookies from './../../config/cookies';

const user = createSlice({
    name: 'auth',
    initialState: {
        token: cookies.getCookie('token'),
        user: {},
        isLoggedIn: JSON.parse(cookies.getCookie('isLoggedIn') || false)
    },
    reducers: {
        login: (state, action) => {
            const auth = action.payload;
            cookies.setCookie('token', auth.token, { expires: 2 });
            cookies.setCookie('userInfo', JSON.stringify(auth.user));
            cookies.setCookie('isLoggedIn', 'true');
            return ({ ...state, token: auth.token, user: auth.user, isLoggedIn: true })

        },
        // logout: (state, action) => {

        //     cookies.removeCookie('token');
        //     cookies.removeCookie('userInfo');
        // }
    }
});

const { reducer, actions } = user;
export const { login, logout } = actions;
export default reducer;