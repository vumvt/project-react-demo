import React from 'react';
import { Row, Col } from 'reactstrap';
import UserApi from '../../api/userApi';
import { toast } from 'react-toastify';
import { useHistory } from 'react-router-dom';
import { Container } from '@material-ui/core';
import LoginForm from './LoginForm';
import cookies from './../../config/cookies';
import { useDispatch } from 'react-redux';
import { login } from './userSlice';

const Login = () => {
    const history = useHistory();
    const dispatch = useDispatch();



    const submit = (data) => {
        UserApi.login({ ...data }).then(res => {
            if (res) {
                history.push('/home');
                // const auth = {
                //     token: res.token,
                //     user: {
                //         username: JSON.stringify(res.username),
                //         email: JSON.stringify(res.email)
                //     },
                //     isLoggedIn: true
                // }
                cookies.setCookie('token', res.token, 2);
                cookies.setCookie('userInfo', JSON.stringify({username: res.username, email: res.email }));
                // dispatch(login(auth));
                toast.success('✔️ Đăng nhập thành công 😀');
            }
            else {
                toast.error('❗ Đăng nhập thất bại rồi ☹️');
            }
        });
    }

    return (
        <Container>
            <Row style={{ alignItems: 'center', marginTop: '13rem' }}>
                <Col sm="12" md={{ size: 5, offset: 3 }} style={{ alignSelf: 'center' }}>
                    <LoginForm submit={submit} />
                </Col>
            </Row>
        </Container>
    );
}

export default Login;