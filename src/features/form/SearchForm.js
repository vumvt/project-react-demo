import { Grid, Button } from '@material-ui/core';
import { FastField, Form, Formik } from 'formik';
import React from 'react';

import * as Yup from "yup";
import { makeStyles } from '@material-ui/core/styles';
import InputField from '../../common/Input/inputField';
import SearchIcon from '@material-ui/icons/Search';


const useStyles = makeStyles((theme) => ({
    container: {
        display: 'grid',
        gridGap: theme.spacing(3),
        marginTop: "5px",
        marginBottom: "5px"

    },
    buttondSearch: {
        marginTop: "10px",
        marginBottom: "10px",
        backgroundColor: "#ffbf00",
        marginLeft: "15px"

    },
    inputSearch: {
        marginTop: "10px",
        marginBottom: "10px",
        marginLeft: "10px"
    },
    divider: {
        margin: theme.spacing(2, 0),
    },
}));
const SearchForm = (props) => {
    const classes = useStyles();
    const { initValues, onSubmit, placeholder } = props;

    const validationSchema = Yup.object().shape({
        searchKey: Yup.string().max(50, 'This field is max length 50').notRequired(),
    });


    return (
        <Formik
            initialValues={initValues}
            validationSchema={validationSchema}
            onSubmit={onSubmit}
            enableReinitialize={true}
        >
            {formikProps => {
                const { isValid, values, errors, resetForm } = formikProps;

                return (
                    <Form autoComplete="off" aria-autocomplete="none">
                        <Grid container xs={12}>
                            <Grid item xs={6}>
                                <FastField
                                    component={InputField}
                                    name="searchKey"
                                    placeholder={placeholder ? placeholder : "Nhập vào từ khóa tìm kiếm ..."}
                                    shrink={true}
                                    fullWidth={true}
                                    className={classes.inputSearch}
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <Button
                                    variant="contained"
                                    type="submit"
                                    size="small"
                                    className={classes.buttondSearch}
                                >
                                    <SearchIcon/>
                                </Button>
                            </Grid>

                        </Grid>

                    </Form>
                )
            }}
        </Formik >
    )
}
export default SearchForm;