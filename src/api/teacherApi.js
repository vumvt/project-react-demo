import axiosClient from './../config/axios';
import baseUrl from './baseUrl';

const teacherApi = {


    getAll: () => { return axiosClient.get(baseUrl.teacher.list); },

    getDetail: (id) => { return axiosClient.get(`${baseUrl.teacher.detail}/${id}`); },

    addNew: (data) => { return axiosClient.post(baseUrl.teacher.add, data); },

    deleteId: (id) => { return axiosClient.delete(`${baseUrl.teacher.delete}/${id}`); },

    update: (data) => { return axiosClient.put(`${baseUrl.teacher.update}/${data.id}`, data); },

    searchKey: (params) => { return axiosClient.get(baseUrl.teacher.searchkey, { params }); }
}
export default teacherApi;