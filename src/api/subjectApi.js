
import axiosClient from './../config/axios';
import baseUrl from './baseUrl';

const subjectApi = {
    getAll: () => { return axiosClient.get(baseUrl.subject.list) },

    getId: (id) => { return axiosClient.get(`${baseUrl.subject.detail}/${id}`) },

    delete: (id) => { return axiosClient.delete(`${baseUrl.subject.delete}/${id}`) },

    add: (data) => { return axiosClient.post(baseUrl.subject.add, data) },

    update: (data) => { return axiosClient.put(`${baseUrl.subject.update}/${data.id}`, data) },

    searchKey: (params) => {return axiosClient.get(baseUrl.subject.searchKey , {params})},

    getSubjectsOfMajorName: (params) => { return axiosClient.get(baseUrl.subject.listSubjectOfMajor, { params }) }
}

export default subjectApi;