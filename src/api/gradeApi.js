import axiosClient from "../config/axios";


const gradeApi = {

    getAll: () => {
        const url = '/grade/list';
        return axiosClient.get(url);
    },

    getId: (id) => {
        const url = `/grade/detail/${id}`;
        return axiosClient.get(url);
    }


}

export default gradeApi;