
import axiosClient from './../config/axios';
import baseUrl from './baseUrl';

const registerApi = {
    getAll: () => { return axiosClient.get(baseUrl.register.list); },
    add: (data) => { return axiosClient.post(baseUrl.register.add, data) },
    update: (data) => { return axiosClient.put(baseUrl.register.update, data) },
    getRegisterByStudentId: (params) => { return axiosClient.get(baseUrl.register.getRegisterByStudentId, { params }) }
}

export default registerApi;