import axiosClient from "../config/axios";
import baseUrl from './baseUrl';


const majorApi = {

    getAll: () => { return axiosClient.get(baseUrl.major.list); },

    getId: (id) => { return axiosClient.get(`${baseUrl.major.detail}/${id}`); },

    add: (data) => { return axiosClient.post(baseUrl.major.add, data); },

    delete: (id) => { return axiosClient.delete(`${baseUrl.major.delete}/${id}`) },

    update: (data) => { return axiosClient.put(`${baseUrl.major.update}/${data.id}`, data); }

}

export default majorApi;