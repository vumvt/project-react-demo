import axiosClient from '../config/axios';
import baseUrl from './baseUrl';

const studentApi = {

    getAll: (params) => { return axiosClient.get(baseUrl.student.list, {params}); },

    getId: (id) => { return axiosClient.get(`${baseUrl.student.detail}/${id}`); },

    addNew: (data) => { return axiosClient.post(baseUrl.student.add, data); },

    delete: (id) => { return axiosClient.delete(`${baseUrl.student.delete}/${id}`); },

    update: (data) => { return axiosClient.put(`${baseUrl.student.update}/${data.id}`, data); },

    getCode: (params) => { return axiosClient.get(baseUrl.student.getCode, { params }) }
}

export default studentApi;