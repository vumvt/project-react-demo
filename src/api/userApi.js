import axiosClient from './../config/axios';
import baseUrl from './baseUrl';

const UserApi = {

    login: (data) => {
        return axiosClient.post(baseUrl.user.signIn, data);
    },

    signUp: (data) => {
        return axiosClient.post(baseUrl.user.signUp, data);
    }
}

export default UserApi;