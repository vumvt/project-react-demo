const baseUrl = {
    teacher: {
        detail: '/teacher/id',
        list: '/teacher/list',
        searchkey: '/teacher/searchKey',
        delete: '/teacher/delete',
        add: '/teacher/add',
        update: '/teacher/update'
    },
    student: {
        list: '/student/list',
        detail: '/student/id',
        add: '/student/add',
        delete: '/student/delete',
        update: '/student/update',
        getCode: '/student/getCode'
    },
    major : {
        list: '/major/list',
        detail: '/major/detail',
        add: '/major/add',
        delete: '/major/delete',
        update: '/major/update'
    },
    user: {
        signIn: '/login',
        signUp: '/signup'
    },
    subject: {
        add: '/subject/add',
        delete: '/subject/delete',
        update: '/subject/update',
        detail: '/subject/id',
        listSubjectOfMajor: '/subject/subjectOfStudentMajorId',
        list: '/subject/list',
        searchKey: '/subject/searchKeySubject'
    },
    register: {
        list: '/subject-registration/list',
        add: '/subject-registration/add',
        update: '/subject-registration/update',
        getRegisterByStudentId: '/subject-registration/getRegisterByStudentId'
    }


}

export default baseUrl;