import { useCookies } from "react-cookie"

const SET_COOKIE = (name, value) => {
    const [cookies, setCookie] = useCookies([name]);
    setCookie(name, value, { path: '/' });
}

const GET_COOKIE = (name) => {
    const [cookies] = useCookies([name]);
    return cookies.name;
}

const REMOVE_COOKIE = (name) => {
    const [removeCookie] = useCookies([name])
    removeCookie(name)
}

const cookie = {
    SET_COOKIE,
    GET_COOKIE,
    REMOVE_COOKIE
}

export default cookie;